package studio.jandesign.orm.android;

import java.lang.reflect.Field;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

public class TableUtil {
	private static final String TAG = "TableUtil";
	private static final String DBTYPE_INTEGER = "Integer";
	private static final String DBTYPE_TEXT = "Text";
	private static final String DBTYPE_REAL = "Real";
	private static final String DBTYPE_BLOB = "Blob";
	private static final String DBTYPE_NUMERIC = "Numeric";
	
	public static String getCreateTableSql(Class<?> cls, String tableName, String pkColName) {
		StringBuilder sql = new StringBuilder("CREATE TABLE " + tableName + " (");
		Field[] fields = cls.getDeclaredFields();
		for (Field f : fields) {
			sql.append(f.getName() + " " + getDBType(f.getType()));
			if (f.getName().equals(pkColName))
				sql.append(" primary key");
			sql.append(", ");
		}
		sql.replace(sql.length() - 2, sql.length(), ")");
		
		return sql.toString();
	}
	
	public static Object toModel(Class<?> cls, Cursor cursor) {
		Object obj = null;
		try {
			obj = cls.newInstance();
			Field[] fields = cls.getDeclaredFields();
			for (Field f : fields) {
				f.setAccessible(true);
				f.set(obj, getValueFromTable(cursor, f));
			}
		} catch (InstantiationException e) {
			Log.e(TAG, e.getMessage());
		} catch (IllegalAccessException e) {
			Log.e(TAG, e.getMessage());
		}
		
		return obj;
	}
	
	public static ContentValues modelToContentValues(Object obj, Class<?> cls) {
		ContentValues values = new ContentValues();
		Field[] fields = cls.getDeclaredFields();
		for (Field f : fields) {
			try {
				setValueFromModel(values, obj, f);
			} catch (IllegalArgumentException e) {
				Log.e(TAG, e.getMessage());
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				Log.e(TAG, e.getMessage());
				e.printStackTrace();
			}
		}
		return values;
	}
	
	private static String getDBType(Class<?> cls) {
		String type = null;
		if (cls.equals(int.class)) {
			type = DBTYPE_INTEGER;
		} else if (cls.equals(String.class)) {
			type = DBTYPE_TEXT;
		} else if (cls.equals(float.class)) {
			type = DBTYPE_REAL;
		} else if (cls.equals(Object.class)) {
			type = DBTYPE_BLOB;
		}
		
		return type;
	}
	
	private static Object getValueFromTable(Cursor cursor, Field col) {
		Object obj = null;
		Class<?> cls = col.getType();
		if (cls.equals(int.class)) {
			obj = cursor.getInt(cursor.getColumnIndex(col.getName()));
		} else if (cls.equals(String.class)) {
			obj = cursor.getString(cursor.getColumnIndex(col.getName()));
		} else if (cls.equals(float.class)) {
			obj = cursor.getFloat(cursor.getColumnIndex(col.getName()));
		}
		
		return obj;
	}
	
	private static void setValueFromModel(ContentValues values, Object obj, Field f) throws IllegalArgumentException, IllegalAccessException {
		Class<?> cls = f.getType();
		f.setAccessible(true);
		if (cls.equals(int.class)) {
			values.put(f.getName(), f.getInt(obj));
		} else if (cls.equals(String.class)) {
			values.put(f.getName(), (String) f.get(obj));
		} else if (cls.equals(float.class)) {
			values.put(f.getName(), f.getFloat(obj));
		}
	}
}

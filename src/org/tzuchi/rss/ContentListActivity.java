package org.tzuchi.rss;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.tzuchi.app.R;
import org.tzuchi.system.DisplayUtil;
import org.tzuchi.system.NetworkUtil;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.app.ActionBar;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ContentListActivity extends ListActivity {
	private TextView textView;
	private String title = "";
	private RssController rssController;
	private ProgressDialog pd;

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		RssItem ns = data.get(position);

		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		bundle.putString("nav_title", title);
		bundle.putString("title", ns.get_title());
		bundle.putString("content", ns.get_desc());
		bundle.putString("link", ns.get_link());
		bundle.putString("img", ns.get_imgURL());
		bundle.putString("imgTitle", ns.get_imgTitle());
		bundle.putInt("id", ns.get_id());
		intent.setClass(ContentListActivity.this, ContentActivity.class);
		intent.putExtras(bundle);
		startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		System.out.println("ContentListActivity onCreate");
		setContentView(R.layout.newslist);

		textView = (TextView) findViewById(R.id.textView1);
		textView.setTextSize(DisplayUtil.getDisplayTextSize());

		pd = new ProgressDialog(this);
		pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		SpannableString spanString = new SpannableString(getString(R.string.loading));
		spanString.setSpan(new RelativeSizeSpan(DisplayUtil.getDisplayRatio() * 1.5f), 0, spanString.length(), 0);
		pd.setMessage(spanString);

		rssController = new RssController(ContentListActivity.this);

		ActionBar actionbar = getActionBar();
		actionbar.setTitle(getResources().getString(R.string.rss_title));
		actionbar.setDisplayHomeAsUpEnabled(true);

		Intent intent = this.getIntent();
		Bundle bundle = intent.getExtras();
		String path = bundle.getString("path");
		int seedId = bundle.getInt("id");
		getRSS(path, seedId);

		// textView.setText(title);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	List<RssItem> data = new ArrayList<RssItem>();

	private List<RssItem> getRSS(final String path, final int seedId) {
		// TODO Auto-generated method stub
		Log.d("=========== ", "getRSS ");
		handler.sendEmptyMessage(0);
		// URL url = null;
		if (NetworkUtil.isNetworkAvailable(ContentListActivity.this)) {
			System.out.println("updateRssSeedList");
			new Thread(path) {
				@Override
				public void run() {
					try {
						URL url = new URL(path);
						SAXParserFactory spf = SAXParserFactory.newInstance();
						SAXParser sp = spf.newSAXParser();
						XMLReader xr = sp.getXMLReader();
						XmlParserHandler myHandler = new XmlParserHandler();
						xr.setContentHandler(myHandler);
						xr.parse(new InputSource(url.openStream()));
						RssController.isMainThreadLocked = true;
						// System.out
						// .println("------------- Locking ------------");
						updateDB(seedId, myHandler);

						while (RssController.isMainThreadLocked)
							;
						// System.out
						// .println("------------- unLocking ------------");
						Cursor cursor = rssController.queryRssSeedById(seedId);
						cursor.moveToNext();
						title = cursor.getString(cursor.getColumnIndex("name"));// myHandler.getRssTitle();
						data = rssController.getRssItemList(seedId);// myHandler.getParserdData();
						// System.out
						// .println("------------- sendEmptyMessage ------------");
						handler.sendEmptyMessage(1); // refresh listview &
														// dismiss
														// progressdialog

					} catch (Exception e) {
						System.out.println("========= Error ============");

						handler.sendEmptyMessage(2);
						e.printStackTrace();
						// goBack();
					}
				}

			}.start();
		} else {
			Cursor cursor = rssController.queryRssSeedById(seedId);
			System.out.println("cursor.getCount() : " + cursor.getCount());
			cursor.moveToNext();
			title = cursor.getString(cursor.getColumnIndex("name"));// myHandler.getRssTitle();
			data = rssController.getRssItemList(seedId);// myHandler.getParserdData();
			if (data.size() == 0) {

				NetworkUtil.showNetworkSetupDialog(ContentListActivity.this);
			}
			handler.sendEmptyMessage(1);
		}

		return data;
	}
	
	private void updateDB(int seedId, XmlParserHandler parserHandler) {
		// TODO Auto-generated method stub
		rssController.updateRssSeedDetail(seedId, parserHandler.getRssFeed());

		rssController.updateRssItemList(seedId, parserHandler.getRssFeed()
				.get_rssItemList());
		RssController.isMainThreadLocked = false;
	}

	// private void showNetworkSetupDialog(){
	// AlertDialog.Builder builder = new
	// AlertDialog.Builder(ContentListActivity.this);
	// builder.setTitle("WiFi连接状态");
	// builder.setMessage("当前网络不可用，是否设置网络.....");
	// builder.setPositiveButton("设定", new DialogInterface.OnClickListener() {
	//
	// @Override
	// public void onClick(DialogInterface dialog, int arg1) {
	//
	// Intent mIntent = new Intent("/");
	// ComponentName comp = new ComponentName("com.android.settings",
	// "com.android.settings.WirelessSettings");
	// mIntent.setComponent(comp);
	// mIntent.setAction("android.intent.action.VIEW");
	// startActivity(mIntent);
	// }
	// });
	// builder.create();
	// builder.show();
	// }

	// //
	// private Handler handler = new Handler() {
	// @Override
	// //
	// public void handleMessage(Message msg) {
	// System.out.println("msg.arg1 : " + msg.arg1);
	// switch (msg.arg1) {
	// case 0:
	// pd.show();
	// break;
	// case 1:
	// System.out.println("------------- pd.dismiss() ------------");
	// textView.setText(title);
	// setListAdapter(new MyAdapter(ContentListActivity.this, data));
	// pd.dismiss();
	// break;
	// }
	// super.handleMessage(msg);
	// }
	// };
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {// 定义一个Handler，用于处理下载线程与UI间通讯
			if (!Thread.currentThread().isInterrupted()) {
				switch (msg.what) {
				case 0:
					pd.show();// 显示进度对话框
					break;
				case 1:
					System.out
							.println("------------- pd.dismiss() ------------");
					textView.setText(title);
					setListAdapter(new MyAdapter(ContentListActivity.this, data));
					pd.dismiss();
					break;
				case 2:

					ImageView errorImg = (ImageView) findViewById(R.id.errorImg);
					errorImg.setVisibility(View.VISIBLE);
					pd.dismiss();
					break;
				}

			}
			super.handleMessage(msg);
		}
	};
}

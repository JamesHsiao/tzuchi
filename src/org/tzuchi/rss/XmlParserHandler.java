package org.tzuchi.rss;

import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.util.Log;

public class XmlParserHandler extends DefaultHandler {
	public static final String DATE_FORMAT_NOW = "EEE, d MMM yyyy HH:mm:ss Z";
	public static final String DATE_FORMAT_DB = "yyyy-MM-dd";
	private boolean inItem = false;
	private boolean inImage = false;

	private RssFeed rssFeed;
	private RssImage rssImage;
	// private List<RssItem> newsList;
	private RssItem news;
	private StringBuffer buf = new StringBuffer();

	public List<RssItem> getParserdData() {
		return rssFeed.get_rssItemList();
	}

	public String getRssTitle() {
		return rssFeed.get_title();// rssFeed.get_title();
	}

	public RssFeed getRssFeed() {
		return rssFeed;
	}

	@Override
	public void startDocument() throws SAXException {
		// TODO Auto-generated method stub
		// System.out.println("startDocument()");
		rssFeed = new RssFeed();
		// newsList = new ArrayList<RssItem>();
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		// System.out.println("startElement " + localName);
		// TODO Auto-generated method stub
		if (localName.equals("item")) {
			this.inItem = true;
			news = new RssItem();

		} else if (localName.equals("image")) {
			rssImage = new RssImage();
			this.inImage = true;
		}

	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		// TODO Auto-generated method stub
		// if (inChannel || inItem || inImage)
		buf.append(ch, start, length);
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		// TODO Auto-generated method stub
		// System.out.println("endElement " + localName);
		if (localName.equals("item")) {
			this.inItem = false;
			rssFeed.addItemToRssItemList(news);
		} else if (localName.equals("title")) {
			if (inItem) {
				news.set_title(buf.toString().trim());
				buf.setLength(0);
			} else if (inImage) {
				rssImage.set_title(buf.toString().trim());
				buf.setLength(0);
			} else {
				rssFeed.set_title(buf.toString().trim());
				buf.setLength(0);
			}
		} else if (localName.equals("link")) {
			if (inItem) {
				news.set_link(buf.toString().trim());
				buf.setLength(0);
			} else if (inImage) {
				rssImage.set_link(buf.toString().trim());
				buf.setLength(0);
			} else {
				rssFeed.set_WebSite(buf.toString().trim());
				buf.setLength(0);
			}
		} else if (localName.equals("description")) {
			if (inItem) {
				String desc = buf.toString().trim();
				// System.out.println("##################################");
				// System.out.println("desc : " + desc);

				if (desc.contains("<img")) {
					int startIndex = desc.indexOf("<img");
					String tmp = desc.substring(startIndex);

					int endIndex = startIndex + tmp.indexOf("/>");
					// System.out.println("startIndex : " +
					// desc.indexOf("<img"));
					// System.out.println("endIndex : " + tmp.indexOf("/>"));
					if (endIndex < 0) {
						endIndex = desc.indexOf("\">");
					}

					String imgSrc = desc.substring(startIndex, endIndex + 2);
					parseContent(imgSrc);
					// System.out.println("imgSrc ==> " + imgSrc);
					desc = desc.replace(imgSrc, "");
					// System.out.println("desc ==> " + desc);
				}
				news.set_desc(desc);

				buf.setLength(0);
			} else {
				rssFeed.set_description(buf.toString().trim());
				buf.setLength(0);
			}
		} else if (localName.equals("pubDate")) {
			if (inItem) {
				String dateTime = buf.toString().trim();

				// System.out.println("dateTime : " + dateTime);

				SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW, Locale.ENGLISH);
				Date today = null;
				try {
					today = sdf.parse(dateTime);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				// System.out.println("today : " + today.toString());
				dateTime = new SimpleDateFormat(DATE_FORMAT_DB).format(today);
				// System.out.println("dateTime : " + dateTime);
				news.set_date(dateTime);

				buf.setLength(0);
			} else {
				rssFeed.set_pubDate(buf.toString().trim());
				buf.setLength(0);
			}

		} else if (localName.equals("language")) {
			rssFeed.set_language(buf.toString().trim());
			buf.setLength(0);

		} else if (localName.equals("url")) {
			if (inImage) {
				rssImage.set_url(buf.toString().trim());
				buf.setLength(0);
			}
		} else if (localName.equals("width")) {
			// if (inImage) {
			// System.out.println("width :  " + buf.toString().trim());
			rssImage.set_width(Integer.parseInt(buf.toString().trim()));
			buf.setLength(0);
			// }
		} else if (localName.equals("height")) {
			// if (inImage) {

			rssImage.set_height(Integer.parseInt(buf.toString().trim()));
			buf.setLength(0);
			// }
		} else if (localName.equals("image")) {
			this.inImage = false;

		} else if (localName.equals("channel")) {
			rssFeed.set_rssImage(rssImage);
		} else {
			// if (inItem) {
			buf.setLength(0);
			// }
		}
	}

	private void parseContent(String desc) {
		// TODO Auto-generated method stub
		try {
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true); // 
												// 
			XmlPullParser parser = factory.newPullParser();

			parser.setInput(new StringReader(desc));

			while (parser.nextTag() == XmlPullParser.START_TAG) {
				String name1 = parser.getName();
				// System.out.println("name1 :" + name1);
				if (name1.equals("img")) {
					// System.out.println("parser.getAttributeValue(null, src) : "
					// + parser.getAttributeValue(null, "src")
					// + " , parser.getAttributeValue(null, title)  : "
					// + parser.getAttributeValue(null, "title"));
					news.set_imgURL(parser.getAttributeValue(null, "src"));
					news.set_imgTitle(parser.getAttributeValue(null, "title"));
				}
			}

		} catch (Exception e) {
			Log.e("XmlParserHandler", e.getMessage());
		}
	}

}

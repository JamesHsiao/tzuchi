package org.tzuchi.rss;

import java.util.List;

import org.tzuchi.system.DisplayUtil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MyAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	private List<RssItem> items;
	private Context context;

	public MyAdapter(Context context, List<RssItem> list) {
		mInflater = LayoutInflater.from(context);
		items = list;
		this.context = context;
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(
					android.R.layout.simple_list_item_1, null);

			holder = new ViewHolder();
			holder.text = (TextView) convertView
					.findViewById(android.R.id.text1);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		RssItem tmpN = (RssItem) items.get(position);
		holder.text.setText(tmpN.get_date() + " : " + tmpN.get_title());
		holder.text.setTextSize(DisplayUtil.getDisplayTextSize());
		holder.text.setTextColor(context.getResources().getColor(android.R.color.black));
		// System.out.println("tmpN.get_title() : "+tmpN.get_title());
		return convertView;
	}

	private class ViewHolder {
		TextView text;
	}

}

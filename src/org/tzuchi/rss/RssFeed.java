package org.tzuchi.rss;

import java.util.ArrayList;
import java.util.List;

public class RssFeed {
	private String _title;
	private String _description;
	private String _WebSite;
	private String _language;
	private String _pubDate;
	private RssImage _rssImage;
	private List<RssItem> _rssItemList;
	
	public RssFeed(){
		_rssItemList = new ArrayList<RssItem>();
	}
	
	public String get_WebSite() {
		return _WebSite;
	}

	public void set_WebSite(String _WebSite) {
		this._WebSite = _WebSite;
	}

	public String get_language() {
		return _language;
	}
	public void set_language(String _language) {
		this._language = _language;
	}
	public String get_title() {
		return _title;
	}
	public void set_title(String _title) {
		this._title = _title;
	}
	public String get_description() {
		return _description;
	}
	public void set_description(String _description) {
		this._description = _description;
	}
	public String get_pubDate() {
		return _pubDate;
	}
	public void set_pubDate(String _pubDate) {
		this._pubDate = _pubDate;
	}
	public RssImage get_rssImage() {
		return _rssImage;
	}
	public void set_rssImage(RssImage _rssImage) {
		this._rssImage = _rssImage;
	}
	public List<RssItem> get_rssItemList() {
		return _rssItemList;
	}
	public void set_rssItemList(List<RssItem> _rssItemList) {
		this._rssItemList = _rssItemList;
	}
	
	public void addItemToRssItemList(RssItem rssItem){
		_rssItemList.add(rssItem);
	}
	
}

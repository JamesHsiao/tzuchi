package org.tzuchi.rss;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.tzuchi.app.R;
import org.tzuchi.system.DisplayUtil;
import org.tzuchi.system.NetworkUtil;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.Html.ImageGetter;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class ContentActivity extends Activity {
	public static final String IMAGE_FORMAT = ".png";
	private TextView titleTV;
	private TextView contentTV;
	// private TextView linkTV;
	private TextView imgTitleTV;
	private ImageView imgContent;

	private String strURL;
	private String imgUrl;
	private String imgTitle;
	String desc;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//System.out.println("ContentActivity onCreate");
		setContentView(R.layout.newscontent);
		
		Intent intent = this.getIntent();
		Bundle bundle = intent.getExtras();
		
		ActionBar actionbar = getActionBar();
		actionbar.setTitle(bundle.getString("nav_title"));
		actionbar.setDisplayHomeAsUpEnabled(true);

		titleTV = (TextView) findViewById(R.id.titleTV);
		titleTV.setTextSize(DisplayUtil.getDisplayHeaderTextSize());
		contentTV = (TextView) findViewById(R.id.ContentTV);
		contentTV.setTextColor(getResources().getColor(R.color.mid_grey));
		float contentTextSize = DisplayUtil.getDisplayTextSize();
		contentTV.setTextSize(contentTextSize);
		imgContent = (ImageView) findViewById(R.id.imgContent);
		imgContent.setClickable(false);
		imgTitleTV = (TextView) findViewById(R.id.imgTitle);
		imgTitleTV.setTextSize(contentTextSize);
		TextView detailTV = (TextView) findViewById(R.id.detailTV);
		detailTV.setTextSize(contentTextSize);
		
		titleTV.setText(bundle.getString("title"));
		imgTitleTV.setText(bundle.getString("imgTitle"));
		desc = bundle.getString("content");
		// System.out.println("Desc : " + desc);

		imgUrl = bundle.getString("img");
		// System.out.println("imgUrl : " + imgUrl);
		imgTitle = bundle.getInt("id") + "";
		setImgContent(imgUrl.replace(" ", "%20"));

		strURL = bundle.getString("link");

		detailTV.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (NetworkUtil.isNetworkAvailable(ContentActivity.this)) {
					Intent intent = new Intent();
					intent.setClass(ContentActivity.this, WebViewActivity.class);
					Bundle bundle = new Bundle();
					bundle.putString("url", strURL);
					intent.putExtras(bundle);
					startActivity(intent);

					// call system default browser
					// Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri
					// .parse(strURL));
					//
					// startActivity(myIntent);
				}
			}

		});

	}

	ImageGetter imgGetter = new Html.ImageGetter() {
		public Drawable getDrawable(String source) {
			Drawable drawable = null;
			Log.d("Image Path", source);
			try {
				drawable = loadImgFromSDCardTest(imgTitle);
			} catch (Exception e) {
				return null;
			}
			if (drawable != null)
				drawable.setBounds(0, 0, drawable.getIntrinsicWidth(),
						drawable.getIntrinsicHeight());
			return drawable;
		}
	};

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void setImgContent(final String imgUrl) {
		// TODO Auto-generated method stub
		if (NetworkUtil.isNetworkAvailable(ContentActivity.this)) {

			if (!imgUrl.equals("") && imgUrl != null) {
				new Thread() {
					@Override
					public void run() {
						// TODO Auto-generated method stub
						try {
							URL url = new URL(imgUrl);
							URLConnection conn = url.openConnection();

							HttpURLConnection httpConn = (HttpURLConnection) conn;
							httpConn.setRequestMethod("GET");
							httpConn.connect();

							if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
								InputStream inputStream = httpConn
										.getInputStream();

								Bitmap bitmap = BitmapFactory
										.decodeStream(inputStream);
								inputStream.close();
								Message msg = new Message();
								// msg.obj = bitmap;
								saveBitmapToSDCard(bitmap, imgTitle);
								bitmap = null;
								myHandler.sendMessage(msg);
							}
						} catch (MalformedURLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				}.start();

			}else{
				imgContent.setVisibility(View.GONE);
				contentTV.setText(Html.fromHtml(desc));
			}
		} else {
			loadImgFromSDCard(imgTitle);
		}

	}

	private Drawable loadImgFromSDCardTest(String filename) {
		// TODO Auto-generated method stub
		String extStorage = Environment.getExternalStorageDirectory()
				.toString();
		File imageFile = new File(extStorage + File.separator + ".tzuchi"
				+ File.separator + filename + IMAGE_FORMAT);
		// System.out.println("imageFile.getAbsolutePath() : "
		// + imageFile.getAbsolutePath());
		if (imageFile.exists())
			return Drawable.createFromPath(imageFile.getAbsolutePath());
		else
			return getResources().getDrawable(R.drawable.error_img_load);
	}

	private void loadImgFromSDCard(String filename) {
		// TODO Auto-generated method stub
		String extStorage = Environment.getExternalStorageDirectory()
				.toString();
		File imageFile = new File(extStorage + File.separator + ".tzuchi"
				+ File.separator + filename + IMAGE_FORMAT);
		// File imageFile = new File("/sdcard/.tzuchi/" + filename + ".png");
		if (!imageFile.exists()) {
			imgContent.setBackgroundResource(R.drawable.error_img_load);
		} else {
			BitmapDrawable d = new BitmapDrawable(getResources(),
					imageFile.getAbsolutePath());
			DisplayMetrics display = DisplayUtil.getDisplayMetrics();
			float scale = (float) display.widthPixels / d.getBitmap().getWidth();
			imgContent.setMaxWidth(d.getBounds().width() * 10);
			imgContent.setMaxHeight(d.getBounds().height() * 10);
			LayoutParams params = new LayoutParams(display.widthPixels, (int) (d.getBitmap().getHeight() * scale));
			params.gravity = Gravity.CENTER;
			imgContent.setLayoutParams(params);
			imgContent.setImageDrawable(d);
		}
	}

	public void saveBitmapToSDCard(Bitmap bitmap, String filename) {
		String extStorage = Environment.getExternalStorageDirectory()
				.toString();
		File dirFile = new File(extStorage + File.separator + ".tzuchi"
				+ File.separator);
		if (!dirFile.exists()) {
			dirFile.mkdirs();
		}
		File file = new File(dirFile.getPath(), filename + IMAGE_FORMAT);
		// System.out.println("saveBitmapToSDCard : " + file.getAbsolutePath());
		try {
			OutputStream outStream = new FileOutputStream(file);
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
			outStream.flush();
			outStream.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Handler myHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			// super.handleMessage(msg);
			// Bitmap img = (Bitmap) msg.obj;
			// imgContent.setImageBitmap(img);
			loadImgFromSDCard(imgTitle);
			contentTV.setText(Html.fromHtml(desc, imgGetter, null));
		}

	};

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

}

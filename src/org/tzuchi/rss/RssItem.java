package org.tzuchi.rss;

public class RssItem {
	private int _id = 0;
	private String _title = "";
	private String _link = "";
	private String _desc = "";
	private String _date = "";
	private String _imgURL = "";
	private String _imgTitle = "";

	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public String get_imgURL() {
		return _imgURL;
	}

	public void set_imgURL(String _imgURL) {
		this._imgURL = _imgURL;
	}

	public String get_title() {
		return _title;
	}

	public void set_title(String title) {
		this._title = title;
	}

	public String get_link() {
		return _link;
	}

	public void set_link(String link) {
		this._link = link;
	}

	public String get_desc() {
		return _desc;
	}

	public void set_desc(String desc) {
		this._desc = desc;
	}

	public String get_date() {
		return _date;
	}

	public void set_date(String _date) {
		this._date = _date;
	}

	public String get_imgTitle() {
		return _imgTitle;
	}

	public void set_imgTitle(String _imgTitle) {
		this._imgTitle = _imgTitle;
	}

}

package org.tzuchi.rss;

import org.tzuchi.app.R;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebViewActivity extends Activity {
	WebView wv;
	ProgressDialog pd;
	Handler handler;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web_content);
		init();// 
		
		ActionBar actionbar = getActionBar();
		actionbar.setDisplayHomeAsUpEnabled(true);

		handler = new Handler() {
			public void handleMessage(Message msg) {// 
				if (!Thread.currentThread().isInterrupted()) {
					switch (msg.what) {
					case 0:
						pd.show();// 
						break;
					case 1:
						pd.hide();// 
						break;
					}
				}
				super.handleMessage(msg);
			}
		};

		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		loadurl(wv, bundle.getString("url"));
		wv.pageDown(true);
	}

	public void init() {// 
		wv = (WebView) findViewById(R.id.webView);
		wv.getSettings().setJavaScriptEnabled(true);// 
		wv.getSettings().setSupportZoom(true);
		wv.getSettings().setBuiltInZoomControls(true);
		wv.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
		wv.setScrollBarStyle(0);// 
		wv.setWebViewClient(new WebViewClient());
		wv.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {// 
				if (progress == 100) {
					handler.sendEmptyMessage(1);// 
				}
				super.onProgressChanged(view, progress);
			}
		});

		pd = new ProgressDialog(this);
		pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		pd.setMessage(getString(R.string.loading));
	}

	public void loadurl(final WebView view, final String url) {
		new Thread() {
			public void run() {
				handler.sendEmptyMessage(0);
				view.loadUrl(url);// 
			}
		}.start();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}

package org.tzuchi.rss;

public class RssImage {
	private String _title;
	private String _url;
	private String _link;
	private int _width;
	private int _height;
	public String get_title() {
		return _title;
	}
	public void set_title(String _title) {
		this._title = _title;
	}
	public String get_url() {
		return _url;
	}
	public void set_url(String _url) {
		this._url = _url;
	}
	public String get_link() {
		return _link;
	}
	public void set_link(String _link) {
		this._link = _link;
	}
	public int get_width() {
		return _width;
	}
	public void set_width(int _width) {
		this._width = _width;
	}
	public int get_height() {
		return _height;
	}
	public void set_height(int _height) {
		this._height = _height;
	}
	
}

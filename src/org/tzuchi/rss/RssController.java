package org.tzuchi.rss;

import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.tzuchi.content.DBHelper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

public class RssController {
	public static String URLPath = "http://tzuchi-001-site1.smarterasp.net/rssfeeds/default.aspx";
	public static boolean isMainThreadLocked = true;
	Context context;
	DBHelper dbHelper;
	SQLiteDatabase db;

	public RssController(Context context) {
		dbHelper = new DBHelper(context);
	}

	public List<RssSeed> getRssSeedList() {

		// TODO Auto-generated method stub
		db = dbHelper.getReadableDatabase();
		Cursor cursor = db.query(DBHelper.TABLE_RSS_FEED, new String[] { "id",
				"name", "url" }, null, null, null, null, null);
		List<RssSeed> seedList = new ArrayList<RssSeed>();
		try {
			while (cursor.moveToNext()) {
				RssSeed seed = new RssSeed();
				seed.set_id(cursor.getInt(cursor.getColumnIndex("id")));
				seed.set_name(cursor.getString(cursor.getColumnIndex("name")));
				seed.set_url(cursor.getString(cursor.getColumnIndex("url")));
				seedList.add(seed);
				// System.out.println(cursor.getString(cursor.getColumnIndex("id"))
				// + " , " + cursor.getString(cursor.getColumnIndex("name"))
				// + " , " + cursor.getString(cursor.getColumnIndex("age")));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cursor != null)
				cursor.close();
		}
		return seedList;
	}

	public Cursor queryRssSeed(RssSeed rssSeed) {
		ContentValues value = new ContentValues();
		value.put("name", rssSeed.get_name());
		value.put("url", rssSeed.get_url());
		db = dbHelper.getReadableDatabase();
		Cursor cursor = db.query(DBHelper.TABLE_RSS_FEED, null, "name=?",
				new String[] { rssSeed.get_name() }, null, null, null);
		return cursor;// cursor.getCount() > 0 ? true : false;
	}

	public Cursor queryRssSeedById(int seedId) {

		db = dbHelper.getReadableDatabase();
		Cursor cursor = db.query(DBHelper.TABLE_RSS_FEED, null, "id=?",
				new String[] { seedId + "" }, null, null, null);

		return cursor;// cursor.getCount() > 0 ? true : false;
	}

	public void updateRssSeed(RssSeed rssSeed) {
		// TODO Auto-generated method stub
		ContentValues value = new ContentValues();
		value.put("name", rssSeed.get_name());
		value.put("url", rssSeed.get_url());
		db = dbHelper.getWritableDatabase();
		db.update(DBHelper.TABLE_RSS_FEED, value, "id=?",
				new String[] { rssSeed.get_id() + "" });

	}

	public void updateRssSeedDetail(int seedId, RssFeed rssFeed) {
		// TODO Auto-generated method stub
		ContentValues value = new ContentValues();
		value.put("title", rssFeed.get_title());
		value.put("desc", rssFeed.get_description());
		value.put("website", rssFeed.get_WebSite());
		value.put("lang", rssFeed.get_language());
		value.put("pubDate", rssFeed.get_pubDate());
		db = dbHelper.getWritableDatabase();
		db.update(DBHelper.TABLE_RSS_FEED, value, "id=?", new String[] { seedId
				+ "" });
	}

	public void deleteRssSeed(RssSeed rssSeed) {
		// TODO Auto-generated method stub
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		// if (index > 1)
		// System.out.println("rssFSeed.get_id()  : " +rssSeed.get_id());
		db.delete(DBHelper.TABLE_RSS_FEED, "id=?",
				new String[] { rssSeed.get_id() + "" });
	}

	// int index = 1;

	public void insertItem(RssSeed rssSeed) {
		// TODO Auto-generated method stub
		ContentValues value = new ContentValues();

		value.put("name", rssSeed.get_name());
		value.put("url", rssSeed.get_url());
		// dbHelper = new DBHelper(DBHelperActivity.this, "test_db",
		// db.getVersion());
		db = dbHelper.getWritableDatabase();
		// System.out.println("insertItem ==> rssSeed.get_name(): "+rssSeed.get_name()
		// +" , rssSeed.get_url() :"+rssSeed.get_url());
		db.insert(DBHelper.TABLE_RSS_FEED, null, value);
		// index++;
	}

	public void updateRssSeedList(final String urlPath) {
		// TODO Auto-generated method stub
		new Thread(urlPath) {
			@Override
			public void run() {
				try {
					URL url = new URL(urlPath);

					DocumentBuilderFactory factory = DocumentBuilderFactory
							.newInstance();
					DocumentBuilder builder = factory.newDocumentBuilder();
					Document document = builder.parse(new InputSource(url
							.openStream()));
					// System.out.println("========= Begin ==========");
					Element root = document.getDocumentElement();
					NodeList nodesList = root.getElementsByTagName("channel");
					// System.out.println("getElementsByTagName : Channel");
					nodesList = root.getElementsByTagName("item");
					// System.out.println("getElementsByTagName : Item ==> "
					// + nodesList.getLength());
					for (int i = 0; i < nodesList.getLength(); i++) {
						RssSeed rssSeed = new RssSeed();
						// System.out.println("======== RssSeed ========");
						Element element = (Element) nodesList.item(i);
						rssSeed.set_name(element.getElementsByTagName("title")
								.item(0).getChildNodes().item(0).getNodeValue());
						// System.out.println("name : " + rssSeed.get_name());
						rssSeed.set_url(element.getElementsByTagName("link")
								.item(0).getChildNodes().item(0).getNodeValue());
						// System.out.println("url : " +rssSeed.get_url());
						// System.out.println("lastBuildDate : "
						// +rssSeed.get_url());
						// undo
						Cursor cursor = queryRssSeed(rssSeed);
						try {
							if (cursor.getCount() > 0)
								updateRssSeed(rssSeed);
							else {
								// System.out
								// .println("updateRssSeedList insertItem(currentRssSeed)");
								insertItem(rssSeed);
							}
						} catch (Exception e) {
							e.printStackTrace();
						} finally {
							if (cursor != null)
								cursor.close();
						}
					}
					// System.out.println("========= End ==========");
					isMainThreadLocked = false;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}

	public void updateRssItemList(int seedId, List<RssItem> rssItemList) {
		// TODO Auto-generated method stub

		for (int i = 0; i < rssItemList.size(); i++) {
			Cursor cursor = queryRssItem(rssItemList.get(i));
			System.out.println("Cursor count : " + cursor.getCount());
			try {
				if (cursor.getCount() > 0) {
					// System.out.println("===== Update =========");
					cursor.moveToNext();
	
					updateRssItem(cursor.getInt(cursor.getColumnIndex("id")),
							rssItemList.get(i));
				} else {
					// System.out.println("===== Insert =========");
					insertRssItem(seedId, rssItemList.get(i));
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (cursor != null)
					cursor.close();
			}
		}
//		 removeOldRssItem();

	}

	private void removeOldRssItem() {
		// TODO Auto-generated method stub

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_YEAR, -60);
		String thresholdDate = new SimpleDateFormat("yyyy-MM-dd").format(cal
				.getTime());
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor cursor = db.query(DBHelper.TABLE_RSS_ITEM,
				new String[] { "id" }, "datetime(date) < datetime('"+ thresholdDate + "')",
				null, null, null, null);
		try {
			while (cursor.moveToNext()) {
				System.out.println("Delete Image ==> "+cursor.getInt(cursor.getColumnIndex("id"))+ContentActivity.IMAGE_FORMAT);
				deleteImgFromSDCard(cursor.getInt(cursor.getColumnIndex("id"))+"");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cursor != null)
				cursor.close();
		}
		
		// System.out.println("thresholdDate: " + thresholdDate);
		db = dbHelper.getWritableDatabase();
		db.delete(DBHelper.TABLE_RSS_ITEM, "datetime(date) < datetime('"
				+ thresholdDate + "')", null);
	}

	private void deleteImgFromSDCard(String filename) {
		// TODO Auto-generated method stub
		String extStorage = Environment.getExternalStorageDirectory()
				.toString();
		File imageFile = new File(extStorage + File.separator + ".tzuchi"
				+ File.separator + filename + ContentActivity.IMAGE_FORMAT);
		// File imageFile = new File("/sdcard/.tzuchi/" + filename + ".png");
		if (imageFile.exists()) {
			imageFile.delete();
		}
	}

	private void updateRssItem(int rssItemId, RssItem rssItem) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		// System.out.println("rssItemId : " + rssItemId);
		ContentValues value = new ContentValues();

		value.put("title", rssItem.get_title());
		value.put("link", rssItem.get_link());
		value.put("desc", rssItem.get_desc());
		value.put("date", rssItem.get_date());
		value.put("imgUrl", rssItem.get_imgURL());
		value.put("imgTitle", rssItem.get_imgTitle());

		// dbHelper = new DBHelper(DBHelperActivity.this, "test_db",
		// db.getVersion());
		db = dbHelper.getWritableDatabase();
		db.update(DBHelper.TABLE_RSS_ITEM, value, "id=?",
				new String[] { rssItemId + "" });
	}

	private Cursor queryRssItem(RssItem rssItem) {
		// TODO Auto-generated method stub

		db = dbHelper.getReadableDatabase();
		Cursor cursor = db.query(DBHelper.TABLE_RSS_ITEM,
				new String[] { "id", "title", "link", "desc", "date", "imgUrl",
						"imgTitle", "seedId" }, "title=?",
				new String[] { rssItem.get_title() }, null, null, null);
		return cursor;// cursor.getCount() > 0 ? true : false;
	}

	public List<RssItem> getRssItemList(int seedId) {
		// TODO Auto-generated method stub

		db = dbHelper.getReadableDatabase();
		Cursor cursor = db.query(DBHelper.TABLE_RSS_ITEM,
				new String[] { "id", "title", "link", "desc", "date", "imgUrl",
						"imgTitle", "seedId" },
				"seedId = ? order by date desc", new String[] { seedId + "" },
				null, null, null);
		List<RssItem> itemList = new ArrayList<RssItem>();
		try {
			while (cursor.moveToNext()) {
				RssItem item = new RssItem();
				item.set_id(cursor.getInt(cursor.getColumnIndex("id")));
				item.set_title(cursor.getString(cursor.getColumnIndex("title")));
				item.set_desc(cursor.getString(cursor.getColumnIndex("desc")));
				item.set_link(cursor.getString(cursor.getColumnIndex("link")));
				item.set_imgURL(cursor.getString(cursor.getColumnIndex("imgUrl")));
				item.set_date(cursor.getString(cursor.getColumnIndex("date")));
				item.set_imgTitle(cursor.getString(cursor
						.getColumnIndex("imgTitle")));
				itemList.add(item);
				// System.out.println(cursor.getString(cursor.getColumnIndex("id"))
				// + " , " + cursor.getString(cursor.getColumnIndex("name"))
				// + " , " + cursor.getString(cursor.getColumnIndex("age")));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cursor != null)
				cursor.close();
		}
		return itemList;
	}

	private void insertRssItem(int seedId, RssItem rssItem) {
		// TODO Auto-generated method stub
		ContentValues value = new ContentValues();

		value.put("title", rssItem.get_title());
		value.put("link", rssItem.get_link());
		value.put("desc", rssItem.get_desc());
		value.put("date", rssItem.get_date());
		value.put("imgUrl", rssItem.get_imgURL());
		value.put("imgTitle", rssItem.get_imgTitle());
		value.put("seedId", seedId);
		// dbHelper = new DBHelper(DBHelperActivity.this, "test_db",
		// db.getVersion());
		db = dbHelper.getWritableDatabase();
		db.insert(DBHelper.TABLE_RSS_ITEM, null, value);
	}

	public void deleteRssItem(int seedId) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		// TODO Auto-generated method stub
		db.delete(DBHelper.TABLE_RSS_ITEM, "seedId=?", new String[] { seedId
				+ "" });
	}

}

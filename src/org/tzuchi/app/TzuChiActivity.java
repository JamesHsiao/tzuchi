package org.tzuchi.app;

import java.util.ArrayList;
import java.util.List;

import org.tzuchi.system.DisplayUtil;

import android.app.ActionBar;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

public class TzuChiActivity extends Activity {
//	private static final String TAG = "TzuChiActivity";
	public static final int ID_TAB_JINGSI = 111;
	public static final int ID_TAB_CALENDAR = 222;
	public static final int ID_TAB_NEWS = 333;
	public static final int ID_TAB_CONTACTS = 444;
	public static final int ID_TAB_MORNING = 555;
	public static final int ID_TAB_SETTINGS = 666;
	public static List<Integer> FRAG_TABS = null;
	private static final String TAG_JINGSI_FRAG = "JingSi";
	private static final String TAG_MORNING_FRAG = "Morning";
	private static final String TAG_CALENDAR_FRAG = "Calendar";
	private static final String TAG_NEWS_FRAG = "News";
	private static final String TAG_CONTACT_FRAG = "Contact";
	private static final String TAG_SETTINGS_FRAG = "Settings";
	private static Button tabJingSi, tabMorning, tabNews, tabCalendar,
			tabContact, tabSettings;
	private static PopupWindow popWindow = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_tzu_chi);

		View actionBarLayout = (View) getLayoutInflater().inflate(
				R.layout.action_bar, null);
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(actionBarLayout);

		initTabs();
		
		ImageButton btnMenu = (ImageButton) actionBarLayout
				.findViewById(R.id.ibMenu);
		btnMenu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!popWindow.isShowing())
					showPopupMenu(v);
				else
					popWindow.dismiss();
			}
		});

		tabJingSi.performClick();
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);

		if (getTabsSumWidth() > DisplayUtil.getDisplayMetrics().widthPixels) {
			LinearLayout actionBarBottom = (LinearLayout) findViewById(R.id.llActionBarBottom);
			View vMenu = (View) getLayoutInflater().inflate(R.layout.popup_menu,
					null);
			while (getTabsSumWidth() > DisplayUtil.getDisplayMetrics().widthPixels) {
				View v = actionBarBottom.getChildAt(actionBarBottom
						.getChildCount() - 1);
				actionBarBottom.removeView(v);
				FRAG_TABS.remove(FRAG_TABS.indexOf(v.getId()));
				addMenu(vMenu, v);
			}
	
			// Initialize menu button
			initPopupMenu(vMenu);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
//		getMenuInflater().inflate(R.menu.activity_tzu_chi, menu);
		return true;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			this.finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	public static PopupWindow getPopWindow() {
		return popWindow;
	}

	private void initTabs() {
		tabJingSi = initTabButton(tabJingSi, ID_TAB_JINGSI,
				getString(R.string.title_jing_si_fragment),
				new TabListener<JingSiFragment>(this, TAG_JINGSI_FRAG,
						JingSiFragment.class));
		tabMorning = initTabButton(tabMorning, ID_TAB_MORNING,
				getString(R.string.title_morning_fragment),
				new TabListener<MorningFragment>(this, TAG_MORNING_FRAG,
						MorningFragment.class));
		tabCalendar = initTabButton(tabCalendar, ID_TAB_CALENDAR,
				getString(R.string.title_calendar_fragment),
				new TabListener<CalendarFragment>(this, TAG_CALENDAR_FRAG,
						CalendarFragment.class));
		tabNews = initTabButton(tabNews, ID_TAB_NEWS,
				getString(R.string.title_rssfeeds_fragment),
				new TabListener<RssFeedsFragment>(this, TAG_NEWS_FRAG,
						RssFeedsFragment.class));
		tabContact = initTabButton(tabContact, ID_TAB_CONTACTS,
				getString(R.string.title_contact_fragment),
				new TabListener<ContactFragment>(this, TAG_CONTACT_FRAG,
						ContactFragment.class));
		tabSettings = initTabButton(tabSettings, ID_TAB_SETTINGS,
				getString(R.string.title_settings_fragment),
				new TabListener<SettingsFragment>(this, TAG_SETTINGS_FRAG,
						SettingsFragment.class));

		FRAG_TABS = new ArrayList<Integer>();
		LinearLayout actionBarBottom = (LinearLayout) findViewById(R.id.llActionBarBottom);
		actionBarBottom.addView(tabJingSi);
		FRAG_TABS.add(ID_TAB_JINGSI);
		actionBarBottom.addView(tabMorning);
		FRAG_TABS.add(ID_TAB_MORNING);
		actionBarBottom.addView(tabCalendar);
		FRAG_TABS.add(ID_TAB_CALENDAR);
		actionBarBottom.addView(tabNews);
		FRAG_TABS.add(ID_TAB_NEWS);
		actionBarBottom.addView(tabContact);
		FRAG_TABS.add(ID_TAB_CONTACTS);
		actionBarBottom.addView(tabSettings);
		FRAG_TABS.add(ID_TAB_SETTINGS);		
	}
	
	private Button initTabButton(Button button, int id, String text,
			TabListener tabListener) {
		button = (Button) getLayoutInflater()
				.inflate(R.layout.tab_button, null);
		button.setId(id);
		button.setTextSize(DisplayUtil.getDisplayTextSize());
		button.setText(text);
		button.setOnClickListener(tabListener);

		return button;
	}

	private void addMenu(View vMenu, View v) {
		LinearLayout llMenu = (LinearLayout) vMenu.findViewById(R.id.llMenu);
		llMenu.addView(v);
	}

	private void initPopupMenu(View vMenu) {
		if (popWindow == null)
			popWindow = new PopupWindow(vMenu, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	}

	private void showPopupMenu(View anchor) {
		if (popWindow != null) {
			popWindow.showAsDropDown(anchor);
		}
	}

	private int getTabsSumWidth() {
		LinearLayout actionBarBottom = (LinearLayout) findViewById(R.id.llActionBarBottom);
		int sumWidth = 0;
		for (int i = 0; i < actionBarBottom.getChildCount(); i++)
			sumWidth += actionBarBottom.getChildAt(i).getWidth();
		return sumWidth;
	}
}

package org.tzuchi.app;

import java.util.List;

import org.tzuchi.rss.ContentListActivity;
import org.tzuchi.rss.RssController;
import org.tzuchi.rss.RssSeed;
import org.tzuchi.system.DisplayUtil;
import org.tzuchi.system.NetworkUtil;

import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class MorningFragment extends Fragment implements OnItemClickListener {
	private MyAdapter adapter;
	private RssController rssController;
	private ListView listView;
	private ImageButton addBtn;
	private Dialog dialog;
	private ProgressDialog pd;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		System.out.println("RssFeedsFragment onCreateView");
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_rss_seedlist, container,
				false);
		initializeUI(view);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		System.out.println("RssFeedsFragment onActivityCreated");
		// Calendar cal = Calendar.getInstance();
		// SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		// System.out.println("Current Date : " + sdf.format(cal.getTime()));
		// System.out.println("onActivityCreated");

		rssController = new RssController(getActivity());
		if (NetworkUtil.isNetworkAvailable(getActivity())) {
			// System.out.println("updateRssSeedList");
			rssController.updateRssSeedList(RssController.URLPath);
		} else {
			System.out.println("Get RssSeed from DB");
			RssController.isMainThreadLocked = false;
			NetworkUtil.showNetworkSetupDialog(getActivity());
		}
		SpannableString spanString = new SpannableString(getString(R.string.loading));
		spanString.setSpan(new RelativeSizeSpan(DisplayUtil.getDisplayRatio() * 1.5f), 0, spanString.length(), 0);
		pd = ProgressDialog.show(getActivity(), null, spanString);
		while (RssController.isMainThreadLocked)
			;
		pd.dismiss();
		// System.out.println("========= unlock ==========");
		adapter = new MyAdapter(getActivity(), rssController);
		listView.setAdapter(adapter);
	}

	private void initializeUI(View view) {
		// TODO Auto-generated method stub
		listView = (ListView) view.findViewById(R.id.rssSeedList);
		addBtn = (ImageButton) view.findViewById(R.id.addBtn);
		addBtn.setOnClickListener(btnClickLsnr);
		listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// TODO Auto-generated method stub
				RssSeed seed = adapter.seedList.get(position);
				showQueDetail(seed);
				return false;
			}

		});
		listView.setOnItemClickListener(this);

		TextView titleRss = (TextView) view.findViewById(R.id.titleTV);
		titleRss.setTextSize(DisplayUtil.getDisplayTitleTextSize());
	}

	private RssSeed currentRssSeed;
	View choiceView;
	private OnClickListener btnClickLsnr = new OnClickListener() {

		@Override
		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.addBtn:
				//
				showQueDetail(null);
				refreshView();
				break;
			case R.id.confirmBtn:
				// RssSeed rssSeed = new RssSeed();
				currentRssSeed.set_name(((EditText) choiceView
						.findViewById(R.id.nameET)).getText().toString());
				currentRssSeed.set_url(((EditText) choiceView
						.findViewById(R.id.urlET)).getText().toString());

				if (currentRssSeed.get_id() > 0) {
					// currentRssSeed.set_id(currentRssSeed.get_id());
					rssController.updateRssSeed(currentRssSeed);
				} else {
					System.out
							.println("confirmBtn rssController.insertItem(currentRssSeed)");
					rssController.insertItem(currentRssSeed);
				}
				dialog.cancel();
				refreshView();
				break;
			case R.id.delBtn:
				rssController.deleteRssSeed(currentRssSeed);
				rssController.deleteRssItem(currentRssSeed.get_id());
				dialog.cancel();
				refreshView();
				break;
			}

		}

	};

	private void refreshView() {
		// TODO Auto-generated method stub
		adapter.getRssSeedList();
		adapter.notifyDataSetChanged();
	}

	private void showQueDetail(RssSeed rssSeed) {
		dialog = new Dialog(getActivity(), R.style.TzuChiRssDialog);
		dialog.setCanceledOnTouchOutside(true);
		LayoutInflater factory = LayoutInflater.from(getActivity());
		choiceView = factory.inflate(R.layout.activity_modified_rssseed,
				(ViewGroup) getActivity().findViewById(R.id.rssSeedInfo));
		float headerSize = DisplayUtil.getDisplayHeaderTextSize();
		TextView textUrl = (TextView) choiceView.findViewById(R.id.tvInputUrl);
		textUrl.setTextSize(headerSize);
		TextView textName = (TextView) choiceView
				.findViewById(R.id.tvInputName);
		textName.setTextSize(headerSize);
		dialog.setContentView(choiceView);

		float btnTextSize = DisplayUtil.getDisplayTextSize();
		if (rssSeed != null) {
			currentRssSeed = rssSeed;
			((EditText) choiceView.findViewById(R.id.nameET)).setText(rssSeed
					.get_name());
			((EditText) choiceView.findViewById(R.id.urlET)).setText(rssSeed
					.get_url());
			Button delBtn = (Button) choiceView.findViewById(R.id.delBtn);
			delBtn.setVisibility(View.VISIBLE);
			delBtn.setOnClickListener(btnClickLsnr);
			delBtn.setTextSize(btnTextSize);
		} else {
			currentRssSeed = new RssSeed();
		}
		Button confirmBtn = (Button) choiceView.findViewById(R.id.confirmBtn);
		confirmBtn.setOnClickListener(btnClickLsnr);
		confirmBtn.setTextSize(btnTextSize);
		dialog.show();

	}

	private class MyAdapter extends BaseAdapter {
		private LayoutInflater mInflater;
		private Context context;
		public List<RssSeed> seedList;// = new ArrayList<RssSeed>();
		// private String[] seedName = { "Udn", "Tzuchi" };
		// private String[] seedURL = {
		// "http://udn.com/udnrss/BREAKINGNEWS1.xml",
		// "http://tzuchi-001-site1.smarterasp.net/services/rss.xml" };
		private RssController rssController;

		public MyAdapter(Context context, RssController rssController) {
			mInflater = LayoutInflater.from(context);
			this.context = context;
			// rssController = new RssController();
			this.rssController = rssController;
			getRssSeedList();
		}

		public void getRssSeedList() {
			seedList = rssController.getRssSeedList();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return seedList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return seedList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			ViewHolder holder;
			if (convertView == null) {
				convertView = mInflater.inflate(
						android.R.layout.simple_list_item_1, null);

				holder = new ViewHolder();
				holder.text = (TextView) convertView
						.findViewById(android.R.id.text1);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			RssSeed tmpN = (RssSeed) seedList.get(position);
			holder.text.setText(tmpN.get_name());
			holder.text.setTextSize(DisplayUtil.getDisplayTextSize());
			holder.text.setTextColor(getResources().getColor(android.R.color.black));
			// System.out.println("tmpN.get_title() : " + tmpN.get_name());
			return convertView;
		}

		private class ViewHolder {
			TextView text;
		}

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,
			long arg3) {
		// System.out.println("========= onItemClick ===========");
		RssSeed seed = adapter.seedList.get(position);
		System.out.println("seed_id : " + seed.get_id());
		Intent intent = new Intent();
		intent.setClass(getActivity(), ContentListActivity.class);
		Bundle bundle = new Bundle();
		bundle.putString("path", seed.get_url());
		bundle.putInt("id", seed.get_id());
		intent.putExtras(bundle);
		startActivityForResult(intent, 0);
	}

}

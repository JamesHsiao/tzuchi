package org.tzuchi.app;

import org.tzuchi.system.DisplayUtil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
public class OpeningAnimationActivity extends Activity {
//	private static final String TAG = "OpeningAnimationActivity";
	private AnimationDrawable openingAnimation;
	private Handler finishHandler;
	public static Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;

		setContentView(R.layout.activity_opening_animation);
		
		// Initialize display parameters
		DisplayUtil.initDisplay(this);

		// Setup opening animation
        ImageView openingImg = (ImageView) findViewById(R.id.animOpening);
        openingImg.setBackgroundResource(R.drawable.opening);
        //openingAnimation = new OpeningAnimationDrawable((AnimationDrawable) getResources().getDrawable(R.drawable.opening));
        openingAnimation = (AnimationDrawable) openingImg.getBackground();
	}

	@Override
    public void onWindowFocusChanged(boolean hasFocus) {
    	super.onWindowFocusChanged(hasFocus);
    	if (hasFocus) {
	        openingAnimation.start();
	        
	        finishHandler = new Handler();
			finishHandler.postDelayed(new Runnable() {
				
				@Override
				public void run() {
					Intent intent = new Intent(context, TzuChiActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					finish();
				}
			}, getTotalDuration(openingAnimation));
    	}
    }
	
	private int getTotalDuration(AnimationDrawable anime) {
		int duration = 0;
		for (int i = 0; i < anime.getNumberOfFrames(); i++) {
			duration += anime.getDuration(i);
		}
		
		return duration;
	}
}

package org.tzuchi.app;

import java.util.List;

import org.tzuchi.jingsi.JingSiConstants;
import org.tzuchi.jingsi.JingSiContentAdapter;
import org.tzuchi.jingsi.JingSiController;
import org.tzuchi.jingsi.JingSiYu;
import org.tzuchi.system.DisplayUtil;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerTitleStrip;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class JingSiYuActivity extends FragmentActivity {
//	private static final String TAG = "JingSiYuActivity";
	private static JingSiController controller;

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_jing_si_yu);
		controller = new JingSiController(this);
		
		ActionBar actionbar = getActionBar();
		actionbar.setDisplayHomeAsUpEnabled(true);
		
		PagerTitleStrip pagerTitleStrip = (PagerTitleStrip) findViewById(R.id.pager_title_strip);
		pagerTitleStrip.setTextSize(TypedValue.COMPLEX_UNIT_SP, DisplayUtil.getDisplayHeaderTextSize());

		Intent intent = getIntent();
		int categoryId = 1;
		if (intent != null) {
			Bundle b = intent.getExtras();
			if (b != null) {
				categoryId = b.getInt(JingSiConstants.CATEGORYID);
			}
		}
		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);
		mViewPager.setCurrentItem(categoryId);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.activity_jing_si_yu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.
			Fragment fragment = new DummySectionFragment();
			Bundle args = new Bundle();
			args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position + 1);
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			// Show all topic pages.
			return controller.getJingSiCategoryCount();
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return controller.getJingSiCategoryById(position + 1);
//			switch (position) {
//			case 0:
//				return getString(R.string.title_section1).toUpperCase();
//			case 1:
//				return getString(R.string.title_section2).toUpperCase();
//			case 2:
//				return getString(R.string.title_section3).toUpperCase();
//			}
//			return null;
		}
	}

	/**
	 * A dummy fragment representing a section of the app, but that simply
	 * displays dummy text.
	 */
	public static class DummySectionFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String ARG_SECTION_NUMBER = "section_number";

		public DummySectionFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			// Create a new TextView and set its text to the fragment's section
			// number argument value.
			ListView listView = new ListView(getActivity());
			int id = getArguments().getInt(ARG_SECTION_NUMBER);
			List<JingSiYu> list = controller.getJingSiYuByCategory(id);
			JingSiContentAdapter adapter = null;
			if (list != null) {
				adapter = new JingSiContentAdapter(getActivity(), list);
			}
			listView.setAdapter(adapter);
			return listView;
		}
	}

}

package org.tzuchi.app;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.tzuchi.jingsi.DailyJingSiAdapter;
import org.tzuchi.jingsi.JingSiConstants;
import org.tzuchi.jingsi.JingSiController;
import org.tzuchi.jingsi.JingSiTopicAdapter;
import org.tzuchi.jingsi.JingSiTranslation;
import org.tzuchi.jingsi.JingSiYu;
import org.tzuchi.service.UpdateJingSiService;
import org.tzuchi.system.DisplayUtil;
import org.tzuchi.system.NetworkUtil;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

public class JingSiFragment extends Fragment implements OnItemClickListener {
	public static final String LANG_ZH = "zh";
	public static final String TAG = "JingSiFragment";
	private static final String PREFS_NAME = "TzuChiPreferences";
	private static final String KEY_UPDATE = "UpdatedDate";
	private static boolean isByTopic = false;
	private static boolean isTodayUpdated = false;
//	private static final String TOPICITEM_ID = "tvJingSiTopic";
	private JingSiController controller = null;
	private GridView categoryView = null;
	private Button btnDisplayType = null;
	private ImageView imgDisplayType = null;
	private int iconSize = 0;

	private OnClickListener btnClickLsnr = new OnClickListener() {

		@Override
		public void onClick(View v) {
			isByTopic = !isByTopic;
			if (isByTopic) {
				readByTopic();
			} else {
				randomJingSi();
			}
		}

	};
	
	private OnClickListener btnUpdateClickLsnr = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			SharedPreferences prefs = getActivity().getSharedPreferences(PREFS_NAME, 0);
			doUpdateService(prefs);
		}
	};
	
	private Handler updateHandler = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == JingSiConstants.CHECK_UPDATE_COMPLETED) {
				stopUpdateAnimation();
			}
		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_jing_si, container, false);
		
		categoryView = (GridView) view.findViewById(R.id.gvJingSi);
		categoryView.setOnItemClickListener(this);

		btnDisplayType = (Button) view.findViewById(R.id.btnDisplayType);
		btnDisplayType.setOnClickListener(btnClickLsnr);
		
		imgDisplayType = (ImageView) view.findViewById(R.id.imgDisplayType);
		imgDisplayType.setOnClickListener(btnClickLsnr);
		
		Button btnUpdate = (Button) view.findViewById(R.id.btnUpdate);
		btnUpdate.setOnClickListener(btnUpdateClickLsnr);
		
		iconSize = DisplayUtil.getDisplayTextHeight(btnDisplayType.getTextSize()) / 2;
		ImageView imgUpdate = (ImageView) view.findViewById(R.id.imgUpdate);
		LayoutParams updateLParams = new LayoutParams(iconSize * 2, iconSize * 2);
		updateLParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		updateLParams.addRule(RelativeLayout.CENTER_VERTICAL);
		updateLParams.setMargins(3, 0, 10, DisplayUtil.getDisplayTextMarginBottom(btnDisplayType.getTextSize()));
		imgUpdate.setLayoutParams(updateLParams);
		imgUpdate.setOnClickListener(btnUpdateClickLsnr);
		
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		controller = new JingSiController(getActivity());
	}
	
	@Override
	public void onResume() {
		if (isByTopic)
			readByTopic();
		else
			randomJingSi();
		
		// Check for updating from web service, once per day
		SharedPreferences prefs = getActivity().getSharedPreferences(PREFS_NAME, 0);
		String upDate = prefs.getString(KEY_UPDATE, null);
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		isTodayUpdated = checkForUpdate(upDate, formatter);
		if (NetworkUtil.isNetworkAvailable(getActivity()) && !isTodayUpdated) {
			// Call background service to check update
			doUpdateService(prefs);
			isTodayUpdated = true;
		}
		super.onResume();
	}

	private void doUpdateService(SharedPreferences prefs) {
		Intent serviceIntent = new Intent(getActivity(), UpdateJingSiService.class);
		serviceIntent.putExtra(JingSiConstants.UPDATE_HANDLER, new Messenger(updateHandler));
		getActivity().startService(serviceIntent);
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		Editor editor = prefs.edit();
		String today = formatter.format(Calendar.getInstance().getTime());
		editor.putString(KEY_UPDATE, today);
		editor.commit();
		
		updateAnimation();
	}
	
	private boolean checkForUpdate(String upDate, SimpleDateFormat formatter) {
		try {
			if (upDate != null) {
				Date d = formatter.parse(upDate);
				Date today = formatter.parse(formatter.format(Calendar.getInstance().getTime()));
				if (!today.after(d) && isTodayUpdated)
					return true;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	private void updateAnimation() {
		Button btnUpdate = (Button) getActivity().findViewById(R.id.btnUpdate);
		if (btnUpdate != null) {
			btnUpdate.setText(R.string.updating);
			btnUpdate.setTextSize(DisplayUtil.getDisplayTextSize());
			Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.update_text);
			btnUpdate.startAnimation(anim);
		}
		ImageView imgUpdate = (ImageView) getActivity().findViewById(R.id.imgUpdate);
		if (imgUpdate != null) {
			Animation rotateAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.update_img);
			imgUpdate.startAnimation(rotateAnim);
		}
	}
	
	private void stopUpdateAnimation() {
		if (getActivity() != null) {
			Button btnUpdate = (Button) getActivity().findViewById(R.id.btnUpdate);
			if (btnUpdate != null) {
				btnUpdate.setText(R.string.update);
				btnUpdate.setTextSize(DisplayUtil.getDisplayTextSize());
				btnUpdate.clearAnimation();
			}
			ImageView imgUpdate = (ImageView) getActivity().findViewById(R.id.imgUpdate);
			if (imgUpdate != null)
				imgUpdate.clearAnimation();
		}
	}
	
	private void randomJingSi() {
		// For ensuring
		if (controller == null) controller = new JingSiController(getActivity());
		JingSiYu jsy = controller.getRandomJingSiYu();
		if (NetworkUtil.isNetworkAvailable(getActivity())) {
			if (jsy == null) {	// The first launch, load data by web service.
				controller.ReadJingSiFromWS(null, JingSiConstants.WSMETHOD_GET_CATEGORIES);
				jsy = controller.getRandomJingSiYu();
			}
		}
		// Get translation aphorisms corresponding to the current display language
		JingSiTranslation jst = null;
		if (jsy != null) {
			String locale = Locale.getDefault().getLanguage();
			if (locale.equals(LANG_ZH)) locale = JingSiConstants.LANG_EN;
			List<JingSiTranslation> jstList = controller.getJingSiTranslation(jsy.getId());
			if (jstList != null) {
				for (JingSiTranslation j : jstList) {
					if (j.getLanguageCode().contains(locale)) {
						jst = j;
						break;
					}
				}
			}
		}
		// Get screen dimension
		DisplayMetrics display = DisplayUtil.getDisplayMetrics();
		categoryView.setColumnWidth(display.widthPixels);
		categoryView.setAdapter(new DailyJingSiAdapter(getActivity(), jsy, jst));
//		categoryView.setBackgroundResource(R.drawable.daily_jingsi_background);
		btnDisplayType.setText(getResources().getText(R.string.bytopic));
		btnDisplayType.setTextSize(DisplayUtil.getDisplayTextSize());
		LayoutParams layoutParams = new LayoutParams(iconSize, iconSize);
		layoutParams.addRule(RelativeLayout.RIGHT_OF, R.id.btnDisplayType);
		layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
		layoutParams.setMargins(5, 0, 0, DisplayUtil.getDisplayTextMarginBottom(btnDisplayType.getTextSize()));
		imgDisplayType.setLayoutParams(layoutParams);
		imgDisplayType.setBackgroundResource(R.drawable.arrow);
	}
	
	private void readByTopic() {
		List<?> list = controller.getJingSiCategory();
		JingSiTopicAdapter adapter = null;
		if (list == null) {	// The first launch, load data by web service.
			if (NetworkUtil.isNetworkAvailable(getActivity())) {
				list = controller.ReadJingSiFromWS(list, JingSiConstants.WSMETHOD_GET_CATEGORIES);
			}
			else {
				NetworkUtil.showNetworkSetupDialog(getActivity());
			}
		}
		else {
			adapter = new JingSiTopicAdapter(getActivity(), list);
		}
		categoryView.setAdapter(adapter);
		categoryView.setBackgroundDrawable(null);
		categoryView.setSelection(0);
		categoryView.setColumnWidth(DisplayUtil.getDisplayMetrics().widthPixels / 4);
		btnDisplayType.setText(getResources().getText(R.string.dailyjingsi));
		btnDisplayType.setTextSize(DisplayUtil.getDisplayTextSize());
		LayoutParams layoutParams = new LayoutParams(iconSize, iconSize);
		layoutParams.addRule(RelativeLayout.RIGHT_OF, R.id.btnDisplayType);
		layoutParams.setMargins(5, 28, 0, 0);
		imgDisplayType.setLayoutParams(layoutParams);
		imgDisplayType.setBackgroundResource(R.drawable.arrow_reverse);
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		
		if (!isByTopic) {
			randomJingSi();
		} else {
			Intent itemIntent = new Intent(getActivity(), JingSiYuActivity.class);
			Bundle b = new Bundle();
			b.putInt(JingSiConstants.CATEGORYID, position);
			itemIntent.putExtras(b);
			startActivity(itemIntent);
		}
	}
}

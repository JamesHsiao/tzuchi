package org.tzuchi.app;

import org.tzuchi.system.DisplayUtil;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.CalendarContract;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class CalendarFragment extends Fragment {
//	private static final String TAG = "CalendarFragment";
	// private Uri calendarUri = CalendarContract.Calendars.CONTENT_URI; //
	// content://com.android.calendar/calendars
	// private ContentResolver calendarResolver =
	// getActivity().getContentResolver();
	// Columns to query
	WebView wv;
	ProgressDialog pd;
	Handler handler;

	private String[] projection = { CalendarContract.Calendars._ID,
			CalendarContract.Calendars.ACCOUNT_NAME,
			CalendarContract.Calendars.ACCOUNT_TYPE,
			CalendarContract.Calendars.NAME,
			CalendarContract.Calendars.CALENDAR_DISPLAY_NAME };
	private final String BOSTON_CALENDAR_URL = "https://www.google.com/calendar/embed?src=tzuchiboston%40gmail.com&ctz=America/New_York";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_calendar, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		init();// 执行初始化函数

		handler = new Handler() {
			public void handleMessage(Message msg) {// 定义一个Handler，用于处理下载线程与UI间通讯
				if (!Thread.currentThread().isInterrupted()) {
					switch (msg.what) {
					case 0:
						pd.show();// 显示进度对话框
						break;
					case 1:
						pd.hide();// 隐藏进度对话框，不可使用dismiss()、cancel(),否则再次调用show()时，显示的对话框小圆圈不会动。
						break;
					}
				}
				super.handleMessage(msg);
			}
		};

		loadurl(wv, BOSTON_CALENDAR_URL);
		wv.pageDown(true);
	}

	public void init() {// 初始化
		wv = (WebView) getActivity().findViewById(R.id.webviewCalendar);
		wv.getSettings().setJavaScriptEnabled(true);// 可用JS
		wv.getSettings().setSupportZoom(true);
		wv.getSettings().setBuiltInZoomControls(true);
		wv.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
		wv.setScrollBarStyle(0);// 滚动条风格，为0就是不给滚动条留空间，滚动条覆盖在网页上
		wv.setWebViewClient(new WebViewClient());
		wv.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {// 载入进度改变而触发
				if (progress == 100) {
					handler.sendEmptyMessage(1);// 如果全部载入,隐藏进度对话框
				}
				super.onProgressChanged(view, progress);
			}
		});

		pd = new ProgressDialog(getActivity());
//		pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		SpannableString spanString = new SpannableString(getString(R.string.loading));
		spanString.setSpan(new RelativeSizeSpan(DisplayUtil.getDisplayRatio() * 1.5f), 0, spanString.length(), 0);
		pd.setMessage(spanString);
	}

	public void loadurl(final WebView view, final String url) {
		new Thread() {
			public void run() {
				handler.sendEmptyMessage(0);
				view.loadUrl(url);// 载入网页
			}
		}.start();
	}

}
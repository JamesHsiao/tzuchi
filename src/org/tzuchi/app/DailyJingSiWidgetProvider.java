package org.tzuchi.app;

import org.tzuchi.jingsi.JingSiConstants;
import org.tzuchi.jingsi.JingSiController;
import org.tzuchi.jingsi.JingSiYu;
import org.tzuchi.system.DisplayUtil;
import org.tzuchi.system.NetworkUtil;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.RemoteViews;

public class DailyJingSiWidgetProvider extends AppWidgetProvider {
	private static final String TAG = "DailyJingSiWidgetProvider";
	private static final long updateInterval = 60 * 1000;
	private static JingSiController controller = null;
	private PendingIntent pending = null;
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		// Initialize JingSi controller if null
		if (controller == null) controller = new JingSiController(context);
		
		// Get all ids
		ComponentName thisWidget = new ComponentName(context, DailyJingSiWidgetProvider.class);
		int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
		for (int widgetId : allWidgetIds) {
			// Get random JingSi
			JingSiYu jsy = controller.getRandomJingSiYu();
			if (jsy == null) {
				if (NetworkUtil.isNetworkAvailable(context)) {
					controller.ReadJingSiFromWS(null, JingSiConstants.WSMETHOD_GET_CATEGORIES);
					jsy = controller.getRandomJingSiYu();
				}
			}
			
			DisplayMetrics display = DisplayUtil.getDisplayMetrics();
			RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.daily_jingsi_widget);
			remoteViews.setTextViewText(R.id.tvWidgetJingSi, jsy != null ? jsy.getAphorisms() : context.getString(R.string.no_jingsi_no_network));
			remoteViews.setFloat(R.id.tvWidgetJingSi, "setTextSize", 20);
			
			// Register an onClickListener
			Intent intent = new Intent(context, OpeningAnimationActivity.class);
//			Intent intent = new Intent(context, DailyJingSiWidgetProvider.class);
//
//			intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
//			intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
//
//			PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
//					0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
			PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
			remoteViews.setOnClickPendingIntent(R.id.llWidgetJingSi, pendingIntent);
			appWidgetManager.updateAppWidget(widgetId, remoteViews);
		}
	}
	
	@Override
	public void onEnabled(Context context) {
		super.onEnabled(context);
		Log.i(TAG, "onEnabled Widget");
		
		ComponentName thisWidget = new ComponentName(context, DailyJingSiWidgetProvider.class);
		final Intent intent = new Intent(context, DailyJingSiWidgetProvider.class);
		intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
		intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, AppWidgetManager.getInstance(context).getAppWidgetIds(thisWidget));
		pending = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		final AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		alarmMgr.cancel(pending);
		alarmMgr.setRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime(), updateInterval, pending);
	}
}

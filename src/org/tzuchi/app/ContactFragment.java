package org.tzuchi.app;

import org.tzuchi.system.DisplayUtil;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class ContactFragment extends Fragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_contact, container, false);
		// Set text size by device resolution.
		float titleSize = DisplayUtil.getDisplayTitleTextSize();
		TextView titleContacts = (TextView) view.findViewById(R.id.tvTitleContacts);
		titleContacts.setTextSize(titleSize);
		
		float headerSize = DisplayUtil.getDisplayHeaderTextSize();
		TextView headerTzuChi = (TextView) view.findViewById(R.id.tvHeaderTzuChi);
		headerTzuChi.setTextSize(headerSize);
		TextView headerBoston = (TextView) view.findViewById(R.id.tvHeaderBoston);
		headerBoston.setTextSize(headerSize);
		TextView headerNewYork = (TextView) view.findViewById(R.id.tvHeaderNewYork);
		headerNewYork.setTextSize(headerSize);
		TextView headerNewJersey = (TextView) view.findViewById(R.id.tvHeaderNewJersey);
		headerNewJersey.setTextSize(headerSize);
		TextView headerLongIsland = (TextView) view.findViewById(R.id.tvHeaderLongIsland);
		headerLongIsland.setTextSize(headerSize);
		
		float contentSize = DisplayUtil.getDisplayTextSize();
		TextView contentTzuChiSite = (TextView) view.findViewById(R.id.tvTzuChiSite);
		contentTzuChiSite.setTextSize(contentSize);
		TextView contentBostonSite = (TextView) view.findViewById(R.id.tvBostonSite);
		contentBostonSite.setTextSize(contentSize);
		TextView contentBostonAddr = (TextView) view.findViewById(R.id.tvBostonAddress);
		contentBostonAddr.setTextSize(contentSize);
		TextView contentBostonPhone = (TextView) view.findViewById(R.id.tvBostonPhone);
		contentBostonPhone.setTextSize(contentSize);
		TextView contentNewYorkAddr = (TextView) view.findViewById(R.id.tvNewYorkAddress);
		contentNewYorkAddr.setTextSize(contentSize);
		TextView contentNewYorkPhone = (TextView) view.findViewById(R.id.tvNewYorkPhone);
		contentNewYorkPhone.setTextSize(contentSize);
		TextView contentNewJerseyAddr = (TextView) view.findViewById(R.id.tvNewJerseyAddress);
		contentNewJerseyAddr.setTextSize(contentSize);
		TextView contentNewJerseyPhone = (TextView) view.findViewById(R.id.tvNewJerseyPhone);
		contentNewJerseyPhone.setTextSize(contentSize);
		TextView contentLongIslandAddr = (TextView) view.findViewById(R.id.tvLongIslandAddress);
		contentLongIslandAddr.setTextSize(contentSize);
		TextView contentLongIslandPhone = (TextView) view.findViewById(R.id.tvLongIslandPhone);
		contentLongIslandPhone.setTextSize(contentSize);
		
		// Set icon size by device resolution
		int iconSize = DisplayUtil.getDisplayIconSize() + DisplayUtil.TEXT_SIZE_BASE;
		ImageView imgTzuChiSite = (ImageView) view.findViewById(R.id.imgTzuChiSite);
		imgTzuChiSite.setLayoutParams(new LayoutParams(iconSize, iconSize));
		ImageView imgBostonSite = (ImageView) view.findViewById(R.id.imgBostonSite);
		imgBostonSite.setLayoutParams(new LayoutParams(iconSize, iconSize));
		ImageView imgBostonAddr = (ImageView) view.findViewById(R.id.imgBostonAddress);
		imgBostonAddr.setLayoutParams(new LayoutParams(iconSize, iconSize));
		ImageView imgNewYorkAddr = (ImageView) view.findViewById(R.id.imgNewYorkAddress);
		imgNewYorkAddr.setLayoutParams(new LayoutParams(iconSize, iconSize));
		ImageView imgNewJerseyAddr = (ImageView) view.findViewById(R.id.imgNewJerseyAddress);
		imgNewJerseyAddr.setLayoutParams(new LayoutParams(iconSize, iconSize));
		ImageView imgLongIslandAddr = (ImageView) view.findViewById(R.id.imgLongIslandAddress);
		imgLongIslandAddr.setLayoutParams(new LayoutParams(iconSize, iconSize));
		
		int phoneIconSize = DisplayUtil.getDisplayPhoneIconSize() + DisplayUtil.TEXT_SIZE_BASE;
		ImageView imgBostonPhone = (ImageView) view.findViewById(R.id.imgBostonPhone);
		imgBostonPhone.setLayoutParams(new LayoutParams(phoneIconSize, phoneIconSize));
		ImageView imgNewYorkPhone = (ImageView) view.findViewById(R.id.imgNewYorkPhone);
		imgNewYorkPhone.setLayoutParams(new LayoutParams(phoneIconSize, phoneIconSize));
		ImageView imgNewJerseyPhone = (ImageView) view.findViewById(R.id.imgNewJerseyPhone);
		imgNewJerseyPhone.setLayoutParams(new LayoutParams(phoneIconSize, phoneIconSize));
		ImageView imgLongIslandPhone = (ImageView) view.findViewById(R.id.imgLongIslandPhone);
		imgLongIslandPhone.setLayoutParams(new LayoutParams(phoneIconSize, phoneIconSize));
		
		return view;
	}
}

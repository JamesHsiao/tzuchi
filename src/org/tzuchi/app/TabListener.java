package org.tzuchi.app;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.PopupWindow;

public class TabListener<T extends Fragment> implements OnClickListener {
//	private final String TAG = this.getClass().getSimpleName();
	private Fragment mFragment;
    private final Activity mActivity;
    private final String mTag;
    private final Class<T> mClass;
 
    /** Constructor used each time a new tab is created.
     * @param activity  The host Activity, used to instantiate the fragment
     * @param tag  The identifier tag for the fragment
     * @param clz  The fragment's Class, used to instantiate the fragment
     */
   public TabListener(Activity activity, String tag, Class<T> clz) {
       mActivity = activity;
       mTag = tag;
       mClass = clz;
   }

	@Override
	public void onClick(View v) {
		for (int id : TzuChiActivity.FRAG_TABS) {
			Button tab = (Button) mActivity.getActionBar().getCustomView().findViewById(id);
			if (id == v.getId()) {
				tab.setSelected(true);
			}
			else {
				tab.setSelected(false);
			}
		}
		PopupWindow popWindow = TzuChiActivity.getPopWindow();
		if (popWindow != null && popWindow.isShowing())
			popWindow.dismiss();
		
		FragmentTransaction fragTrans = mActivity.getFragmentManager().beginTransaction();
		// Check if the fragment is already initialized
		if (mFragment == null) {
			// If not, instantiate and add it to the activity
			mFragment = Fragment.instantiate(mActivity, mClass.getName());
			mFragment.setRetainInstance(true);
			fragTrans.add(R.id.frameFragment, mFragment, mTag);
			fragTrans.commit();
		} else {
			// If it exists, simply attach it in order to show it
			fragTrans.replace(R.id.frameFragment, mFragment, mTag);
			fragTrans.addToBackStack(null);
			fragTrans.commitAllowingStateLoss();
		}
	}
}

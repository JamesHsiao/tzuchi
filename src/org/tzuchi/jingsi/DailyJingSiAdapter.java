package org.tzuchi.jingsi;

import org.tzuchi.app.R;
import org.tzuchi.system.DisplayUtil;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class DailyJingSiAdapter extends BaseAdapter {
//	private static final String TAG = "DailyJingSiAdapter";
	private Activity activity = null;
	private JingSiYu jingSiYu = null;
	private JingSiTranslation jingSiTrans = null;

	public DailyJingSiAdapter(Activity activity, JingSiYu jingSiYu, JingSiTranslation jingSiTran) {
		this.activity = activity;
		if (jingSiYu != null)
			this.jingSiYu = jingSiYu;
		else {
			JingSiYu jsy = new JingSiYu();
			jsy.setAphorisms(activity.getResources().getText(R.string.no_jingsi_no_network).toString());
			this.jingSiYu = jsy;
		}
		this.jingSiTrans = jingSiTran;
	}
	
	@Override
	public int getCount() {
		return 1;
	}

	@Override
	public Object getItem(int idx) {
		// TODO Auto-generated method stub
		return jingSiYu;
	}

	@Override
	public long getItemId(int idx) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = activity.getLayoutInflater();
		View dailyItem = inflater.inflate(R.layout.daily_jingsi_item, parent, false);
		
//		RelativeLayout rLayout = new RelativeLayout(activity);
//		rLayout.setLayoutParams(new GridView.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
//		rLayout.setGravity(Gravity.CENTER);
//		rLayout.setBackgroundResource(R.drawable.daily_jingsi_background);
		DisplayMetrics display = DisplayUtil.getDisplayMetrics();
		int textSize = (int)DisplayUtil.getDisplayTextSize();
		
		// JingSiYu
		TextView txtView = (TextView) dailyItem.findViewById(R.id.tvDailyJingSi);
//		TextView txtView = new TextView(activity);
//		txtView.setId(DESCIRPT_ID);
//		txtView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		txtView.setWidth(display.widthPixels * 2 / 3);
//		txtView.setTextColor(activity.getResources().getColor(android.R.color.white));
		txtView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
		txtView.setText(jingSiYu.getAphorisms());
		
//		rLayout.addView(txtView);

		// JingSiTranslation
		if (jingSiTrans != null) {
			TextView transView = (TextView) dailyItem.findViewById(R.id.tvJingSiTranslate);
//			TextView transView = new TextView(activity);
//			transView.setId(TRANS_ID);
//			LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
//			params.addRule(RelativeLayout.BELOW, DESCIRPT_ID);
//			params.topMargin = 10;
//			transView.setLayoutParams(params);
			transView.setWidth(display.widthPixels * 2 / 3);
//			transView.setTextColor(activity.getResources().getColor(R.color.light_blue));
			transView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize - 2);
			transView.setText(jingSiTrans.getTranslation());
			
//			rLayout.addView(transView);
		}
		
		if (jingSiYu.getLastModified() != null) {
			TextView descirptView = (TextView) dailyItem.findViewById(R.id.tvJingSiFrom);
//			TextView descirptView = new TextView(activity);
//			LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
//			params.addRule(RelativeLayout.BELOW, jingSiTrans != null ? TRANS_ID : DESCIRPT_ID);
//			params.topMargin = 10;
//			descirptView.setLayoutParams(params);
//			descirptView.setTextColor(activity.getResources().getColor(R.color.light_blue_2));
//			descirptView.setTypeface(null, Typeface.BOLD);
			descirptView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize - 4);
//			descirptView.setText(activity.getResources().getText(R.string.jingsi_description));
			
//			rLayout.addView(descirptView);
		}
		
		return dailyItem;
//		return rLayout;
	}

}

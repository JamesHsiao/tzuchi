package org.tzuchi.jingsi;

import java.util.List;

import org.tzuchi.app.R;
import org.tzuchi.system.DisplayUtil;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.AbsListView.LayoutParams;
import android.widget.TextView;

public class JingSiTopicAdapter extends BaseAdapter {
//	private static final String TAG = "JingSiTopicAdapter";
	private Activity activity = null;
	private List<JingSiCategory> jingSiYuCategories = null;

	@SuppressWarnings("unchecked")
	public JingSiTopicAdapter(Activity activity, List<?> jingSiYuCategories) {
		this.activity = activity;
		if (jingSiYuCategories != null)
			this.jingSiYuCategories = (List<JingSiCategory>) jingSiYuCategories;
	}
	
	@Override
	public int getCount() {
		return jingSiYuCategories.size();
	}

	@Override
	public Object getItem(int idx) {
		// TODO Auto-generated method stub
		return jingSiYuCategories.get(idx);
	}

	@Override
	public long getItemId(int idx) {
		// TODO Auto-generated method stub
		return idx;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = activity.getLayoutInflater();
		View topicItem = inflater.inflate(R.layout.jingsi_topic_item, parent, false);
		int gridMetrics = DisplayUtil.getDisplayMetrics().widthPixels / 4;
		topicItem.setLayoutParams(new LayoutParams(gridMetrics, gridMetrics));
		
		// JingSiYu
		TextView txView = (TextView) topicItem.findViewById(R.id.tvJingSiTopic);
		txView.setTextSize(DisplayUtil.getDisplayTextSize() - 2);
		txView.setText(jingSiYuCategories.get(position).getCategory());
		
		return topicItem;
	}

}

package org.tzuchi.jingsi;

public class JingSiYu {
	private int id;
	private String aphorisms;
	private int categoryId;
	private String lastModified;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getAphorisms() {
		return aphorisms;
	}

	public void setAphorisms(String aphorisms) {
		this.aphorisms = aphorisms;
	}
	
	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getLastModified() {
		return lastModified;
	}

	public void setLastModified(String createdDate) {
		this.lastModified = createdDate;
	}
	
	public String toString() {
		return aphorisms;
	}
}

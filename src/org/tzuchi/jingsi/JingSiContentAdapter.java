package org.tzuchi.jingsi;

import java.util.List;

import org.tzuchi.app.R;
import org.tzuchi.system.DisplayUtil;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class JingSiContentAdapter extends BaseAdapter {
//	private static final String TAG = "JingSiContentAdapter";
	private Activity activity = null;
	private List<JingSiYu> jingSiYus = null;

	@SuppressWarnings("unchecked")
	public JingSiContentAdapter(Activity activity, List<?> jingSiYus) {
		this.activity = activity;
		if (jingSiYus != null)
			this.jingSiYus = (List<JingSiYu>) jingSiYus;
	}
	
	@Override
	public int getCount() {
		return jingSiYus.size();
	}

	@Override
	public Object getItem(int idx) {
		// TODO Auto-generated method stub
		return jingSiYus.get(idx);
	}

	@Override
	public long getItemId(int idx) {
		// TODO Auto-generated method stub
		return idx;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = activity.getLayoutInflater();
		View contentItem = inflater.inflate(R.layout.jingsi_content_item, parent, false);
		
		// JingSiYu
		TextView txView = (TextView) contentItem.findViewById(R.id.tvJingSiContent);
		txView.setTextSize(DisplayUtil.getDisplayTextSize() - 2);
		txView.setText(jingSiYus.get(position).getAphorisms());
		
		return contentItem;
	}

}

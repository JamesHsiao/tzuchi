package org.tzuchi.jingsi;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.tzuchi.app.JingSiFragment;
import org.tzuchi.content.DBHelper;
import org.xmlpull.v1.XmlPullParserException;

import studio.jandesign.orm.android.TableUtil;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

public class JingSiController {
	private static final String TAG = "JingSiController";
	private SQLiteDatabase db;
	private DBHelper dbHelper;

	public JingSiController(Context context) {
		dbHelper = new DBHelper(context);
	}

	// Web service operations
	public class CallWebServiceTask extends	AsyncTask<Integer, Integer, String> {
	
		@Override
		protected String doInBackground(Integer... params) {
			SoapObject request = null;
			String soapAction = null;
			switch (params[0]) {
			case JingSiConstants.WSMETHOD_GET_CATEGORIES:
				request = new SoapObject(JingSiConstants.NAMESPACE, JingSiConstants.GET_ALL_CATEG_METHOD);
				soapAction = JingSiConstants.NAMESPACE + JingSiConstants.GET_ALL_CATEG_METHOD;
				break;
			case JingSiConstants.WSMETHOD_GET_ALLJINGSIYU:
				request = new SoapObject(JingSiConstants.NAMESPACE, JingSiConstants.GET_ALL_JINGSI_METHOD);
				soapAction = JingSiConstants.NAMESPACE + JingSiConstants.GET_ALL_JINGSI_METHOD;
				break;
			}
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER12);
			envelope.dotNet = true;
			envelope.bodyOut = request;
			// Register envelope
			(new MarshalBase64()).register(envelope);
		
			HttpTransportSE transport = new HttpTransportSE(JingSiConstants.WEBSERVICEURL);
			transport.debug = true;
			try {
				transport
						.setXmlVersionTag(JingSiConstants.XML_VERSION_1_0_ENCODING_UTF_8);
				transport.call(soapAction, envelope);
				return transport.responseDump;
			} catch (IOException e) {
				Log.e(TAG, e.getMessage());
				e.printStackTrace();
			} catch (XmlPullParserException e) {
				Log.e(TAG, e.getMessage());
				e.printStackTrace();
			}
		
			return null;
		}
	
	}
	
	public List<?> ReadJingSiFromWS(List<?> list, int callWSMethod) {
		AsyncTask<Integer, Integer, String> task = new CallWebServiceTask();
		task.execute(callWSMethod);
		JingSiXmlParser parser = new JingSiXmlParser();
		try {
			parser.parseAllJingSi(task.get());
			insertDatabase(parser);
		} catch (InterruptedException e) {
			Log.e(JingSiFragment.TAG, e.getMessage());
			e.printStackTrace();
		} catch (ExecutionException e) {
			Log.e(JingSiFragment.TAG, e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	private void insertDatabase(JingSiXmlParser parser) {
		List<JingSiCategory> jingSiCategoryList = parser.getJingSiCategories();
		insertJingSiCategory(jingSiCategoryList.toArray(new JingSiCategory[jingSiCategoryList.size()]));
		List<JingSiYu> jingSiYuList = parser.getJingSiYu();
		insertJingSiYu(jingSiYuList.toArray(new JingSiYu[jingSiYuList.size()]));
		List<JingSiTranslation> translationList = parser.getJingSiTranslations();
		insertJingSiTranslation(translationList.toArray(new JingSiTranslation[translationList.size()]));
	}
	
	// DB operations
	public int getJingSiCategoryCount() {
		db = dbHelper.getReadableDatabase();
		Cursor cursor = db.query(DBHelper.TABLE_JINGSI_CATEGORY,
				new String[] { "categoryId" }, null, null, null, null, null);
		int count = 0;
		try {
			count = cursor.getCount();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cursor != null)
				cursor.close();
		}
		db.close();
		return count;
	}

	public String getJingSiCategoryById(int id) {
		db = dbHelper.getReadableDatabase();
		Cursor cursor = db.query(DBHelper.TABLE_JINGSI_CATEGORY,
				new String[] { "category" },
				"categoryId=" + String.valueOf(id), null, null, null, null);
		String category = "";
		try {
			if (cursor.moveToNext()) {
				category = cursor.getString(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cursor != null)
				cursor.close();
		}
		db.close();
		return category;
	}

	public List<JingSiCategory> getJingSiCategory() {
		db = dbHelper.getReadableDatabase();
		Cursor cursor = db.query(DBHelper.TABLE_JINGSI_CATEGORY, null, null,
				null, null, null, null);
		List<JingSiCategory> list = null;
		try {
			if (cursor.getCount() > 0) {
				list = new ArrayList<JingSiCategory>();
				while (cursor.moveToNext()) {
					list.add((JingSiCategory) TableUtil.toModel(JingSiCategory.class,
							cursor));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cursor != null)
				cursor.close();
		}
		db.close();

		return list;
	}

	public String getLastModifiedDateByTable(String tableName) {
		String lastModified = null;
		db = dbHelper.getReadableDatabase();
		Cursor cursor = db.query(tableName,
				new String[] { JingSiConstants.COL_LAST_MODIFIED }, null, null,
				null, null, JingSiConstants.COL_LAST_MODIFIED + " "
						+ JingSiConstants.DESC, "1");
		try {
			if (cursor.moveToNext()) {
				SimpleDateFormat sdf = new SimpleDateFormat(
						JingSiConstants.INPUT_DATETIME_FORMAT, Locale.US);
				Date date = sdf.parse(cursor.getString(cursor
						.getColumnIndex(JingSiConstants.COL_LAST_MODIFIED)));
				sdf.applyPattern(JingSiConstants.OUTPUT_DATETIME_FORMAT);
				lastModified = sdf.format(date);
			}
		} catch (ParseException e) {
			Log.e(TAG, e.getMessage());
		} finally {
			cursor.close();
		}
		db.close();

		return lastModified;
	}

	private void insertJingSiCategory(JingSiCategory... categories) {
		db = dbHelper.getWritableDatabase();
		for (JingSiCategory jsc : categories) {
			ContentValues values = TableUtil.modelToContentValues(jsc,
					jsc.getClass());
			db.insert(DBHelper.TABLE_JINGSI_CATEGORY, null, values);
		}
		db.close();
	}

	public void updateJingSiCategory(JingSiCategory... categories) {
		db = dbHelper.getWritableDatabase();
		for (JingSiCategory jsc : categories) {
			ContentValues values = TableUtil.modelToContentValues(jsc,
					jsc.getClass());
			int noRow = db.update(DBHelper.TABLE_JINGSI_CATEGORY, values, "categoryId=?",
						new String[] { String.valueOf(jsc.getCategoryId()) });
			if (noRow == 0) db.insert(DBHelper.TABLE_JINGSI_CATEGORY, null, values);
		}
		db.close();
	}

	public JingSiYu getRandomJingSiYu() {
		db = dbHelper.getReadableDatabase();
		Cursor cursor = db.query(DBHelper.TABLE_JINGSIYU, null, null, null,
				null, null, "RANDOM()", "1");

		JingSiYu jsy = null;
		try {
			if (cursor.moveToNext()) {
				jsy = (JingSiYu) TableUtil.toModel(JingSiYu.class, cursor);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cursor != null)
				cursor.close();
		}
		db.close();

		return jsy;
	}

	public List<JingSiYu> getJingSiYuByCategory(int categoryId) {
		db = dbHelper.getReadableDatabase();
		Cursor cursor = db.query(DBHelper.TABLE_JINGSIYU, null, "categoryId=?",
				new String[] { String.valueOf(categoryId) }, null, null, null);
		List<JingSiYu> list = null;

		try {
			if (cursor.getCount() > 0) {
				list = new ArrayList<JingSiYu>();
				while (cursor.moveToNext()) {
					list.add((JingSiYu) TableUtil.toModel(JingSiYu.class, cursor));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cursor != null)
				cursor.close();
		}
		db.close();

		return list;
	}

	private void insertJingSiYu(JingSiYu... jingSiYus) {
		db = dbHelper.getWritableDatabase();
		for (JingSiYu jsy : jingSiYus) {
			ContentValues values = TableUtil.modelToContentValues(jsy,
					jsy.getClass());
			db.insert(DBHelper.TABLE_JINGSIYU, null, values);
		}
		db.close();
	}

	public void updateJingSiYu(JingSiYu... jingSiYus) {
		db = dbHelper.getWritableDatabase();
		for (JingSiYu jsy : jingSiYus) {
			ContentValues values = TableUtil.modelToContentValues(jsy,
					jsy.getClass());
			int noRow = db.update(DBHelper.TABLE_JINGSIYU, values, "id=?",
					new String[] { String.valueOf(jsy.getId()) });
			if (noRow == 0) db.insert(DBHelper.TABLE_JINGSIYU, null, values);
		}
		db.close();
	}

	private void insertJingSiTranslation(JingSiTranslation... jingSiTranslations) {
		db = dbHelper.getWritableDatabase();
		for (JingSiTranslation jst : jingSiTranslations) {
			ContentValues values = TableUtil.modelToContentValues(jst,
					jst.getClass());
			db.insert(DBHelper.TABLE_JINGSI_TRANSLATION, null, values);
		}
		db.close();
	}

	public void updateJingSiTranslation(JingSiTranslation... jingSiTranslations) {
		db = dbHelper.getWritableDatabase();
		for (JingSiTranslation jst : jingSiTranslations) {
			ContentValues values = TableUtil.modelToContentValues(jst,
					jst.getClass());
			int noRow = db.update(DBHelper.TABLE_JINGSI_TRANSLATION, values, "trans_Id=?",
					new String[] { String.valueOf(jst.getTrans_Id()) });
			if (noRow == 0) db.insert(DBHelper.TABLE_JINGSI_TRANSLATION, null, values);
		}
		db.close();
	}

	public List<JingSiTranslation> getJingSiTranslation(int jingSiYuId) {
		db = dbHelper.getReadableDatabase();
		Cursor cursor = db.query(DBHelper.TABLE_JINGSI_TRANSLATION, null, "jingSiYuId=?",
				new String[] { String.valueOf(jingSiYuId) }, null, null, null);
		List<JingSiTranslation> list = null;
		
		try {
			if (cursor.getCount() > 0) {
				list = new ArrayList<JingSiTranslation>();
				while (cursor.moveToNext()) {
					list.add((JingSiTranslation) TableUtil.toModel(JingSiTranslation.class, cursor));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cursor != null)
				cursor.close();
		}
		db.close();

		return list;
	}
	
}

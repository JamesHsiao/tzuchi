package org.tzuchi.jingsi;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.util.Log;


public class JingSiXmlParser {
	private final String TAG = "GetJingSiXmlParser";
	private final String KEY_JINGSI_CATEGORY = "CategoryVO";
	private final String KEY_JINGSI_UPDATED_CATEGORY = "CategoryUpdatedVO";
	private final String KEY_JINGSIYU = "JingSiYuVO";
	private final String KEY_JINGSIYU_UPDATED = "JingSiYuUpdatedVO";
	private final String KEY_ID = "Id";
	private final String KEY_CATEGORY_ID = "CategoryId";
	private final String KEY_JINGSIYU_ID = "ParentJingSiYuId";
	private final String KEY_NAME = "Name";
	private final String KEY_NAME_UPDATED = "Category";
	private final String KEY_APHORISMS = "Aphorisms";
	private final String KEY_LANGCODE = "LanguageCode";
	private final String KEY_TRANSLATIONS = "JingSiYuTransVO";
	private final String KEY_LASTMODIFIED = "LastModified";
	private final String LANG_ZHTW = "zh-tw";
	private ArrayList<JingSiCategory> categories;
	private ArrayList<JingSiYu> jingSiYus;
	private ArrayList<JingSiTranslation> jingSiTranslations;
	
	public JingSiXmlParser () {
		categories = new ArrayList<JingSiCategory>();
		jingSiYus = new ArrayList<JingSiYu>();
		jingSiTranslations = new ArrayList<JingSiTranslation>();
	}
	
	public ArrayList<JingSiCategory> getJingSiCategories() {
		return categories;
	}
	
	public ArrayList<JingSiYu> getJingSiYu() {
		return jingSiYus;
	}
	
	public ArrayList<JingSiTranslation> getJingSiTranslations() {
		return jingSiTranslations;
	}
	
	public void parseAllJingSi(String xml) {
		Document doc = getDomElement(xml);
		
		NodeList nlCategory = doc.getElementsByTagName(KEY_JINGSI_CATEGORY);
		for (int i = 0; i < nlCategory.getLength(); i++) {
			Element e = (Element) nlCategory.item(i);
			JingSiCategory category = new JingSiCategory();
			int categoryId = Integer.valueOf(getValue(e, KEY_ID));
			category.setCategoryId(categoryId);
			category.setCategory(getValue(e, KEY_NAME));
			category.setLastModified(getValue(e, KEY_LASTMODIFIED).replace("T", " "));
			categories.add(category);
			
			// Read aphorisms
			readJingSiYu(e, categoryId);
		}
	}

	// Read aphorisms
	private void readJingSiYu(Element e, int categoryId) {
		NodeList nlAphorism = e.getElementsByTagName(KEY_JINGSIYU);
		for (int j = 0; j < nlAphorism.getLength(); j++) {
			Element ea = (Element) nlAphorism.item(j);
			JingSiYu jsy = new JingSiYu();
			int jsyId = Integer.valueOf(getValue(ea, KEY_ID));
			jsy.setId(jsyId);
			jsy.setAphorisms(getValue(ea, KEY_APHORISMS));
			jsy.setCategoryId(categoryId);
			jsy.setLastModified(getValue(ea, KEY_LASTMODIFIED).replace("T", " "));
			jingSiYus.add(jsy);
			
			// Read other language
			readJingSiTranslations(ea, jsyId);
		}
	}

	// Read other language
	private void readJingSiTranslations(Element ea, int jsyId) {
		NodeList nlOtherLang = ea.getElementsByTagName(KEY_TRANSLATIONS);
		if (nlOtherLang.getLength() == 0) {
			Log.w(TAG, "No jingsiyu translations !!");
			return;
		}
		for (int k = 0; k < nlOtherLang.getLength(); k++) {
			Element eml = (Element) nlOtherLang.item(k);
			String langCode = getValue(eml, KEY_LANGCODE);
			if (langCode != null && !langCode.equals(LANG_ZHTW)) {
				JingSiTranslation jst = new JingSiTranslation();
				jst.setTrans_Id(Integer.parseInt(getValue(eml, KEY_ID)));
				jst.setJingSiYuId(jsyId);
				jst.setLanguageCode(langCode);
				jst.setTranslation(getValue(eml, KEY_APHORISMS));
				jst.setLastModified(getValue(eml, KEY_LASTMODIFIED).replace("T", " "));
				jingSiTranslations.add(jst);
			}
		}
	}
	
	public void parseJingSiCategories(String xml) {
		Document doc = getDomElement(xml);
		
		NodeList nlCategory = doc.getElementsByTagName(KEY_JINGSI_UPDATED_CATEGORY);
		for (int i = 0; i < nlCategory.getLength(); i++) {
			Element e = (Element) nlCategory.item(i);
			JingSiCategory category = new JingSiCategory();
			int categoryId = Integer.valueOf(getValue(e, KEY_ID));
			category.setCategoryId(categoryId);
			category.setCategory(getValue(e, KEY_NAME_UPDATED));
			category.setLastModified(getValue(e, KEY_LASTMODIFIED).replace("T", " "));
			categories.add(category);
		}
	}
	
	public void parseJingSiYu(String xml) {
		Document doc = getDomElement(xml);
		
		NodeList nlAphorism = doc.getElementsByTagName(KEY_JINGSIYU_UPDATED);
		for (int j = 0; j < nlAphorism.getLength(); j++) {
			Element ea = (Element) nlAphorism.item(j);
			JingSiYu jsy = new JingSiYu();
			int jsyId = Integer.valueOf(getValue(ea, KEY_ID));
			jsy.setId(jsyId);
			jsy.setAphorisms(getValue(ea, KEY_APHORISMS));
			jsy.setCategoryId(Integer.parseInt(getValue(ea, KEY_CATEGORY_ID)));
			jsy.setLastModified(getValue(ea, KEY_LASTMODIFIED).replace("T", " "));
			jingSiYus.add(jsy);
		}
	}
	
	public void parseJingSiTranslation(String xml) {
		Document doc = getDomElement(xml);
		
		NodeList nlOtherLang = doc.getElementsByTagName(KEY_TRANSLATIONS);
		if (nlOtherLang.getLength() == 0) {
			Log.w(TAG, "No jingsiyu translations !!");
			return;
		}
		for (int k = 0; k < nlOtherLang.getLength(); k++) {
			Element eml = (Element) nlOtherLang.item(k);
			String langCode = getValue(eml, KEY_LANGCODE);
			if (langCode != null && !langCode.equals(LANG_ZHTW)) {
				JingSiTranslation jst = new JingSiTranslation();
				jst.setTrans_Id(Integer.parseInt(getValue(eml, KEY_ID)));
				jst.setJingSiYuId(Integer.parseInt(getValue(eml, KEY_JINGSIYU_ID)));
				jst.setLanguageCode(langCode);
				jst.setTranslation(getValue(eml, KEY_APHORISMS));
				jst.setLastModified(getValue(eml, KEY_LASTMODIFIED).replace("T", " "));
				jingSiTranslations.add(jst);
			}
		}
	}
	
	private Document getDomElement(String xml) {
		Document doc = null;
		DocumentBuilderFactory docBuilderFatory = DocumentBuilderFactory.newInstance();
		
		try {
			DocumentBuilder docBuilder = docBuilderFatory.newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xml));
			doc = docBuilder.parse(is);
		} catch (ParserConfigurationException e) {
			Log.e(TAG, e.getMessage());
			return null;
		} catch (SAXException e) {
			Log.e(TAG, e.getMessage());
			return null;
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
			return null;
		}
		
		return doc;
	}
	
	private String getValue(Element e, String elementName) {
		NodeList nodeList = e.getElementsByTagName(elementName);
		return getElementValue(nodeList.item(0));
	}
	
	private final String getElementValue(Node node) {
		Node child;
		if (node != null && node.hasChildNodes()) {
			for (child = node.getFirstChild(); child != null; child = child.getNextSibling()) {
				if (child.getNodeType() == Node.TEXT_NODE) {
					return child.getNodeValue();
				}
			}
		}
		
		return "";
	}
}

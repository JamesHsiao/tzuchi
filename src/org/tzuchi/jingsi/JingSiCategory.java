package org.tzuchi.jingsi;


public class JingSiCategory {
	private int categoryId;
	private String category;
	private String lastModified;
	
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int id) {
		this.categoryId = id;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	public String toString() {
		return category;
	}
	public String getLastModified() {
		return lastModified;
	}
	public void setLastModified(String createdDate) {
		this.lastModified = createdDate;
	}
}

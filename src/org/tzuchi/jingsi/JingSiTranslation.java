package org.tzuchi.jingsi;

public class JingSiTranslation {
	private int trans_Id;
	private String translation;
	private String languageCode;
	private int jingSiYuId;
	private String lastModified;
	
	public int getTrans_Id() {
		return trans_Id;
	}
	public void setTrans_Id(int trans_Id) {
		this.trans_Id = trans_Id;
	}
	public String getTranslation() {
		return translation;
	}
	public void setTranslation(String translation) {
		this.translation = translation;
	}
	public String getLanguageCode() {
		return languageCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	public int getJingSiYuId() {
		return jingSiYuId;
	}
	public void setJingSiYuId(int jingSiYuId) {
		this.jingSiYuId = jingSiYuId;
	}
	public String getLastModified() {
		return lastModified;
	}
	public void setLastModified(String createdDate) {
		this.lastModified = createdDate;
	}
}

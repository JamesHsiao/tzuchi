package org.tzuchi.jingsi;


public class JingSiConstants {	
	public static final String CATEGORYID = "categoryId";
	public static final String BUNDLEEXTRANAME = "tzuchi.intent.extra.jingsi";
	public static final String COL_LAST_MODIFIED = "lastModified";
	public static final String DESC = "DESC";
	public static final String INPUT_DATETIME_FORMAT = "yyyy-MM-dd hh:mm:ss";
	public static final String OUTPUT_DATETIME_FORMAT = "MM/dd/yyyy";
	public static final String WEBSERVICEURL = "http://tzuchi-001-site1.smarterasp.net/webservices/jingsiYu.asmx";
	public static final String NAMESPACE = "http://tempuri.org/";
	// Web service methods
	public static final int WSMETHOD_GET_CATEGORIES = 1;
	public static final int WSMETHOD_GET_ALLJINGSIYU = 2;
	public static final int WSMETHOD_GET_LASTMODIFIED_CATEG = 3;
	public static final int WSMETHOD_GET_LASTMODIFIED_JINGSI = 4;
	public static final int WSMETHOD_GET_LASTMODIFIE_JINGSI_TRANS = 5;
	public static final String GET_ALL_CATEG_METHOD = "GetJingSiCategory";
	public static final String GET_ALL_JINGSI_METHOD = "GetAllJingSiYu";
	public static final String GET_LAST_MOD_CATEG_METHOD = "GetLastModifiedCategory";
	public static final String GET_LAST_MOD_JINGSI_METHOD = "GetLastModifiedJingSiYu";
	public static final String GET_LAST_MOD_JINGSITRANS_METHOD = "GetLastModifiedJingSiYuTranslation";
	public static final String PARAM_LAST_MODIFIED_DATE = "lastModifiedDate";
	// UI constants
	public static final int TOPIC_COLUMN_WIDHT = 105;
	public static final String XML_VERSION_1_0_ENCODING_UTF_8 = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
	public static final String LANG_EN = "en";
	// Messages
	public static final int CHECK_UPDATE_COMPLETED = 1983;
	public static final String UPDATE_HANDLER = "UpdateHandler";
}

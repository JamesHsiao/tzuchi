package org.tzuchi.system;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtil {

	public static boolean isNetworkAvailable(Context context) {
	
		ConnectivityManager connectivity = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {// 
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {// 
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public static void showNetworkSetupDialog(final Activity activity){
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setTitle("Network status");
		builder.setMessage("This network is unavailable. Please check ur setting.");
		builder.setPositiveButton("Setting", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int arg1) {

				Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
				intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				activity.startActivity(intent);
				activity.finish();
			}
		});
		builder.create();
		builder.show();
	}
	


}

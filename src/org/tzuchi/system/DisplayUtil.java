package org.tzuchi.system;

import android.app.Activity;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.util.DisplayMetrics;

public class DisplayUtil {
	public static final int TEXT_SIZE_BASE = 18;
	private static final int TITLE_TEXT_SIZE_BASE = 26;
	private static final int SUBTITLE_TEXT_SIZE_BASE = 18;
	private static final int HEADER_TEXT_SIZE_BASE = 20;
	private static final int ICON_SIZE_BASE = 20;
	private static final int PHONE_ICON_SIZE_BASE = 18;
	public static final int DISPLAY_COMPARE_WIDTH_BASE = 480;
	private static DisplayMetrics displayMetrics = new DisplayMetrics();
	private static float displayRatio = 1;

	public static void initDisplay(Activity activity) {
		activity.getWindowManager().getDefaultDisplay()
				.getMetrics(displayMetrics);
		displayRatio = displayMetrics.widthPixels / (float)DISPLAY_COMPARE_WIDTH_BASE;
	}

	public static DisplayMetrics getDisplayMetrics() {
		return displayMetrics;
	}
	
	public static int getDisplayTextHeight(float textSize) {
		Paint paint = new Paint();
		paint.setTextSize(textSize);
		FontMetrics fm = paint.getFontMetrics();
		return (int) Math.ceil(fm.descent - fm.ascent);
	}
	
	public static int getDisplayTextMarginBottom(float textSize) {
		Paint paint = new Paint();
		paint.setTextSize(textSize);
		FontMetrics fm = paint.getFontMetrics();
		return (int) fm.descent;
	}
	
	public static float getDisplayTextSize() {
		return displayRatio * TEXT_SIZE_BASE;
	}
	
	public static float getDisplayTitleTextSize() {
		return displayRatio * TITLE_TEXT_SIZE_BASE;
	}
	
	public static float getDisplaySubTitleTextSize() {
		return displayRatio * SUBTITLE_TEXT_SIZE_BASE;
	}
	
	public static float getDisplayHeaderTextSize() {
		return displayRatio * HEADER_TEXT_SIZE_BASE;
	}
	
	public static int getDisplayIconSize() {
		return (int) (displayRatio * ICON_SIZE_BASE);
	}
	
	public static int getDisplayPhoneIconSize() {
		return (int) (displayRatio * PHONE_ICON_SIZE_BASE);
	}
	
	public static float getDisplayRatio() {
		return displayRatio;
	}
}

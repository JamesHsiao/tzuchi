package org.tzuchi.service;

import java.io.IOException;
import java.util.List;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.tzuchi.content.DBHelper;
import org.tzuchi.jingsi.JingSiCategory;
import org.tzuchi.jingsi.JingSiConstants;
import org.tzuchi.jingsi.JingSiController;
import org.tzuchi.jingsi.JingSiTranslation;
import org.tzuchi.jingsi.JingSiXmlParser;
import org.tzuchi.jingsi.JingSiYu;
import org.xmlpull.v1.XmlPullParserException;

import android.app.IntentService;
import android.content.Intent;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

public class UpdateJingSiService extends IntentService {
	private static final String TAG = "UpdateJingSiService";
	
	public UpdateJingSiService() {
		super(TAG);
	}
	
	@Override
	protected void onHandleIntent(Intent intent) {
		// Do update
		String xml = getUpdateFromWebService(JingSiConstants.WSMETHOD_GET_LASTMODIFIED_CATEG);
		JingSiController controller = new JingSiController(this);
		JingSiXmlParser parser = new JingSiXmlParser();
		if (xml != null) {
			parser.parseJingSiCategories(xml);
			List<JingSiCategory> list = parser.getJingSiCategories();
			Log.i(TAG, "JingSiCategory size: " + list.size());
			controller.updateJingSiCategory(list.toArray(new JingSiCategory[list.size()]));
		}
		xml = getUpdateFromWebService(JingSiConstants.WSMETHOD_GET_LASTMODIFIED_JINGSI);
		if (xml != null) {
			Log.i(TAG, "Update JingSiYu");
			parser.parseJingSiYu(xml);
			List<JingSiYu> list = parser.getJingSiYu();
			controller.updateJingSiYu(list.toArray(new JingSiYu[list.size()]));
		}
		xml = getUpdateFromWebService(JingSiConstants.WSMETHOD_GET_LASTMODIFIE_JINGSI_TRANS);
		if (xml != null) {
			Log.i(TAG, "Update JingSiTranslation");
			parser.parseJingSiTranslation(xml);
			List<JingSiTranslation> list = parser.getJingSiTranslations();
			controller.updateJingSiTranslation(list.toArray(new JingSiTranslation[list.size()]));
		}
		
		// Send complete message
		Messenger messenger = (Messenger) intent.getExtras().get(JingSiConstants.UPDATE_HANDLER);
		Message msg = Message.obtain();
		msg.what = JingSiConstants.CHECK_UPDATE_COMPLETED;
		try {
			messenger.send(msg);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private String getUpdateFromWebService(int wsMethod) {
		SoapObject request = null;
		String soapAction = null;
		switch (wsMethod) {
		case JingSiConstants.WSMETHOD_GET_LASTMODIFIED_CATEG:
			request = new SoapObject(JingSiConstants.NAMESPACE, JingSiConstants.GET_LAST_MOD_CATEG_METHOD);
			soapAction = JingSiConstants.NAMESPACE + JingSiConstants.GET_LAST_MOD_CATEG_METHOD;
			break;
		case JingSiConstants.WSMETHOD_GET_LASTMODIFIED_JINGSI:
			request = new SoapObject(JingSiConstants.NAMESPACE, JingSiConstants.GET_LAST_MOD_JINGSI_METHOD);
			soapAction = JingSiConstants.NAMESPACE + JingSiConstants.GET_LAST_MOD_JINGSI_METHOD;
			break;
		case JingSiConstants.WSMETHOD_GET_LASTMODIFIE_JINGSI_TRANS:
			request = new SoapObject(JingSiConstants.NAMESPACE, JingSiConstants.GET_LAST_MOD_JINGSITRANS_METHOD);
			soapAction = JingSiConstants.NAMESPACE + JingSiConstants.GET_LAST_MOD_JINGSITRANS_METHOD;
			break;
		}
		request.addProperty(JingSiConstants.PARAM_LAST_MODIFIED_DATE, getLocalLastModifiedDate(wsMethod));
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER12);
		envelope.dotNet = true;
		envelope.bodyOut = request;
		// Register envelope
		(new MarshalBase64()).register(envelope);
		
		HttpTransportSE transport = new HttpTransportSE(JingSiConstants.WEBSERVICEURL);
		transport.debug = true;
		transport.setXmlVersionTag(JingSiConstants.XML_VERSION_1_0_ENCODING_UTF_8);
		try {
			transport.call(soapAction, envelope);
			return transport.responseDump;
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		
		return null;
	}

	private String getLocalLastModifiedDate(int wsMethod) {
		String tableName = null;
		switch (wsMethod) {
		case JingSiConstants.WSMETHOD_GET_LASTMODIFIED_CATEG:
			tableName = DBHelper.TABLE_JINGSI_CATEGORY;
			break;
		case JingSiConstants.WSMETHOD_GET_LASTMODIFIED_JINGSI:
			tableName = DBHelper.TABLE_JINGSIYU;
			break;
		case JingSiConstants.WSMETHOD_GET_LASTMODIFIE_JINGSI_TRANS:
			tableName = DBHelper.TABLE_JINGSI_TRANSLATION;
			break;
		}
		JingSiController controller = new JingSiController(this);
		
		return controller.getLastModifiedDateByTable(tableName);
	}
}

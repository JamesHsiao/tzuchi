package org.tzuchi.content;

import org.tzuchi.jingsi.JingSiCategory;
import org.tzuchi.jingsi.JingSiTranslation;
import org.tzuchi.jingsi.JingSiYu;

import studio.jandesign.orm.android.TableUtil;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
	private static final int VERSION = 1;
	public static final String DB_NAME = "TzuChi.db";
	public static final String TABLE_JINGSI_CATEGORY = "JingSi_Category";
	public static final String TABLE_JINGSIYU = "JingSiYu";
	public static final String TABLE_JINGSI_TRANSLATION = "JingSi_Translation";
	public static final String TABLE_RSS_FEED = "RssFeed";
	public static final String TABLE_RSS_ITEM = "RssItem";

	public DBHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
	}

	public DBHelper(Context context) {

		super(context, DB_NAME, null, VERSION);
		// System.out.println("========== DBHelper ===========");
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// System.out.println("========== onCreate ===========");
		if (db != null) {
			db.execSQL(TableUtil.getCreateTableSql(JingSiCategory.class,
					TABLE_JINGSI_CATEGORY, "categoryId"));
			db.execSQL(TableUtil.getCreateTableSql(JingSiYu.class,
					TABLE_JINGSIYU, "id"));
			db.execSQL(TableUtil.getCreateTableSql(JingSiTranslation.class,
					TABLE_JINGSI_TRANSLATION, "trans_Id"));
			db.execSQL("create table "
					+ TABLE_RSS_FEED
					+ "(id Integer primary key ,name text,url text,title text,desc text,website text,lang text,"
					+ "pubDate text)");
			db.execSQL("create table "
					+ TABLE_RSS_ITEM
					+ "(id Integer primary key ,title text,link text,desc text,date text,imgUrl text,imgTitle text,"
					+ "seedId INTEGER NOT NULL ,FOREIGN KEY (seedId) REFERENCES "
					+ TABLE_RSS_FEED + " (id))");

			db.execSQL("CREATE TRIGGER fk_itemseed_seedid " + " BEFORE INSERT "
					+ " ON " + TABLE_RSS_ITEM + " FOR EACH ROW BEGIN"
					+ " SELECT CASE WHEN ((SELECT id FROM " + TABLE_RSS_FEED
					+ " WHERE id =new.seedId) IS NULL)"
					+ " THEN RAISE (ABORT,'Foreign Key Violation') END;"
					+ "  END;");
			// db.execSQL("CREATE TABLE "
			// + TABLE_JINGSIYU
			// +
			// " (Id Integer primary key, aphorisms nvarchar(1000), categoryId Integer");
			// db.execSQL("CREATE TABLE "
			// + TABLE_JINGSI_TRANSLATION
			// +
			// " (trans_Id Integer primary key, translation nvarchar(1000), languageCode varchar(50), jingSiYuId Integer, sort Integer");
			// db.execSQL("CREATE TABLE "
			// + TABLE_RSS_FEED
			// +
			// " (id Integer primary key, name varchar(20), url varchar(256))");
			// db.execSQL("CREATE TABLE "
			// + TABLE_RSS_FEED
			// +
			// " (rssId Integer primary key, title nvarchar(500), link varchar(500), description nvarchar(8000), category nvarchar(500), sort Integer");
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXIST " + TABLE_JINGSI_CATEGORY);
		db.execSQL("DROP TABLE IF EXIST " + TABLE_JINGSIYU);
		db.execSQL("DROP TABLE IF EXIST " + TABLE_JINGSI_TRANSLATION);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_RSS_FEED);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_RSS_ITEM);
		// db.execSQL("DROP TABLE IF EXIST " + TABLE_RSS_FEED);
		onCreate(db);
	}

}

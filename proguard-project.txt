# To enable ProGuard in your project, edit project.properties
# to define the proguard.config property as described in that file.
#
# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in ${sdk.dir}/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the ProGuard
# include property in project.properties.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-dontwarn android.support.**
-keepclassmembers class org.tzuchi.jingsi.JingSiCategory {
	public *;
	private *;
}
-keepclassmembers class org.tzuchi.jingsi.JingSiYu {
	public *;
	private *;
}
-keepclassmembers class org.tzuchi.jingsi.JingSiTranslation {
	public *;
	private *;
}
-keep class org.xmlpull.v1.XmlPullParserFactory {
	public *;
}
-keep class org.xmlpull.v1.XmlPullParser {
	public *;
}

-ignorewarnings
-libraryjars libs/ksoap2-android-assembly-3.0.0-RC.4-jar-with-dependencies.jar